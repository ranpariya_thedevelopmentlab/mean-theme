'use strict';

var gulp = require('gulp'),
	gulpsync = require('gulp-sync')(gulp),
	less = require('gulp-less'),
	cssmin = require('gulp-cssmin'),
	header = require('gulp-header'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	lessToScss = require('gulp-less-to-scss'),
	concat = require('gulp-concat'),
	util = require('gulp-util'),
	notify = require("gulp-notify");

var jsBanner = [  
	'/*********************************************',
	'* Minified Javascript for Ranpariya Theme',
	'* All kind of basic Javascription transmission.',
	'*',
	'* Developer: Ranpariya - The development lab',
	'*********************************************/',
	'\n'].join('\n');

var cssBanner = [  
	'/*********************************************',
	'* Minified CSS for Ranpariya Theme',
	'* All kind of basic css.',
	'*',
	'* Developer: Ranpariya - The development lab',
	'*********************************************/',
	'\n'].join('\n');

var sassBanner = [  
	'/*********************************************',
	'* Minified SASS for Ranpariya Theme',
	'* All kind of basic sass.',
	'*',
	'* Developer: Ranpariya - The development lab',
	'*********************************************/',
	'\n'].join('\n');

var path = {
	js: {
		src: './app/src/assets/js/**/*.js',
		dest: './app/src/assets/dist/js'
	},
	css: {
		src: './app/src/assets/less/**/*.less',
		dest: './app/src/assets/dist/css'
	},
	lessToScss:{
		src: './app/src/assets/less/**/*.less',
		dest: './app/src/assets/sass'
	},
	fonts: {
		src: './app/src/assets/fonts/**/*',
		dest: './app/src/assets/dist/fonts'
	}
}

// --
// Environments
var production = util.env.production;

// --
// Error handler
function onError(err) {
  console.log(err);
  this.emit('end');
}

// --
// Watch task
gulp.task('compile-watch', function() {
	gulp.watch(path.css.src, gulpsync.sync(['copyfonts', 'lessToCSS', 'concat-css']));
	gulp.watch(path.js.src, gulpsync.sync(['js-minify']));
});

// --
// Js minify
gulp.task('js-minify', function() {
	return gulp.src(path.js.src)
		.pipe(production ? uglify().on('error', onError) : util.noop())
		.pipe(rename({suffix: '.min'}))
		.pipe(header(jsBanner))
		.pipe(gulp.dest(path.js.dest))
		.pipe(notify({
			title   : 'Gulp Task Complete',
			message : 'Javascript have been compiled',
			onLast: true
		}));
});

//--
// Less to scss
gulp.task('lessToScss',function(){
    gulp.src(path.lessToScss.src)
        .pipe(lessToScss().on('error', onError))
        .pipe(gulp.dest(path.lessToScss.dest));
});

// --
// Less compile and minify css
gulp.task('lessToCSS', function() {
	return gulp.src(['./app/src/assets/less/themes/strawberry/strawberry-theme.less',
		'./app/src/assets/less/themes/orange/orange-theme.less',
		'./app/src/assets/less/themes/lime/lime-theme.less',
		'./app/src/assets/less/themes/blueberry/blueberry-theme.less',
		'./app/src/assets/less/themes/almond/almond-theme.less',
		'./app/src/assets/less/themes/fresh-apple/fresh-apple-theme.less',
		'./app/src/assets/less/themes/teal/teal-theme.less',
		'./app/src/assets/less/themes/elderberry/elderberry-theme.less',

		'./app/src/assets/less/themes/strawberry-light/strawberry-light-theme.less',
		'./app/src/assets/less/themes/orange-light/orange-light-theme.less',
		'./app/src/assets/less/themes/lime-light/lime-light-theme.less',
		'./app/src/assets/less/themes/almond-light/almond-light-theme.less',
		'./app/src/assets/less/themes/fresh-apple-light/fresh-apple-light-theme.less',
		'./app/src/assets/less/themes/teal-light/teal-light-theme.less',
		'./app/src/assets/less/themes/blueberry-light/blueberry-light-theme.less',
		'./app/src/assets/less/themes/elderberry-light/elderberry-light-theme.less',

		'./app/src/assets/less/theme-pages/authenticate.less'])
		.pipe(less().on('error', onError))
		.pipe(production ? cssmin().on('error', onError) : util.noop())
		.pipe(rename({suffix: '.min'}))
		.pipe(header(cssBanner))
		.pipe(gulp.dest(path.css.dest));
});

//--
// Sass to css
gulp.task('sassToCSS', function() {
	return gulp.src(['./sass/themes/strawberry/strawberry-theme.scss',
		'./sass/themes/orange/orange-theme.scss',
		'./sass/themes/lime/lime-theme.scss',
		'./sass/themes/blueberry/blueberry-theme.scss',
		'./sass/themes/almond/almond-theme.scss',
		'./sass/themes/fresh-apple/fresh-apple-theme.scss',
		'./sass/themes/teal/teal-theme.scss',
		'./sass/themes/elderberry/elderberry-theme.scss',

		'./sass/themes/strawberry-light/strawberry-light-theme.scss',
		'./sass/themes/orange-light/orange-light-theme.scss',
		'./sass/themes/lime-light/lime-light-theme.scss',
		'./sass/themes/almond-light/almond-light-theme.scss',
		'./sass/themes/fresh-apple-light/fresh-apple-light-theme.scss',
		'./sass/themes/teal-light/teal-light-theme.scss',
		'./sass/themes/blueberry-light/blueberry-light-theme.scss',
		'./sass/themes/elderberry-light/elderberry-light-theme.scss',

		'./sass/theme-pages/authenticate.scss'])
		.pipe(sass().on('error', onError))
		.pipe(production ? cssmin().on('error', onError) : util.noop())
		.pipe(rename({suffix: '.min'}))
		.pipe(header(sassBanner))
		.pipe(gulp.dest(path.css.dest));
});

// --
// Concat css file
gulp.task('concat-css', function() {
	var css = gulp.src(['./app/src/assets/dist/css/blueberry-theme.min.css',
		'./app/src/assets/dist/css/orange-theme.min.css',
		'./app/src/assets/dist/css/lime-theme.min.css',
		'./app/src/assets/dist/css/strawberry-theme.min.css',
		'./app/src/assets/dist/css/almond-theme.min.css',
		'./app/src/assets/dist/css/fresh-apple-theme.min.css',
		'./app/src/assets/dist/css/teal-theme.min.css',
		'./app/src/assets/dist/css/elderberry-theme.min.css',
		'./app/src/assets/dist/css/strawberry-light-theme.min.css',
		'./app/src/assets/dist/css/orange-light-theme.min.css',
		'./app/src/assets/dist/css/lime-light-theme.min.css',
		'./app/src/assets/dist/css/almond-light-theme.min.css',
		'./app/src/assets/dist/css/fresh-apple-light-theme.min.css',
		'./app/src/assets/dist/css/teal-light-theme.min.css',
		'./app/src/assets/dist/css/blueberry-light-theme.min.css',
		'./app/src/assets/dist/css/elderberry-light-theme.min.css'])
		.pipe(concat('app.min.css'))
		.pipe(gulp.dest(path.css.dest))
		.pipe(notify({
			title   : 'Gulp Task Complete',
			message : 'Styles have been compiled',
			onLast: true
		}));
});

// --
// Fonts
gulp.task('copyfonts', function() {
	gulp.src(path.fonts.src)
	.pipe(gulp.dest(path.fonts.dest));
});

gulp.task('default', gulpsync.sync(['copyfonts', 'lessToCSS', 'concat-css', 'js-minify']));
gulp.task('watch', gulpsync.sync(['copyfonts', 'lessToCSS', 'concat-css', 'js-minify', 'compile-watch']));