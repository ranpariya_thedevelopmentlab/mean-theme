// app.js

// MODULES =================================================
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var session = require('express-session');
var validator = require('express-validator');

// CONFIGURATIONS =================================================

// --
// Config files
var db = require('./config/db');


// connect to  mongoDB  
mongoose.connect(db.url);

// --
// Create our app w/ express
var app = express();

app.use(validator());

// --
// Set the static files location /public/img will be /img for users
app.use(express.static(path.join(__dirname, 'app/dist')));

// --
// View engine setup
// app.set('views', path.join(__dirname, 'views'));
app.set('view engine','ejs'); // Set EJS View Engine
app.engine('html', require('ejs').renderFile); // Set HTML engine

// --
// Log every request to the console
app.use(logger('dev'));

// --
// Get all data/stuff of the body (POST) parameters
// Parse request bodies
// Parse application/json
app.use(bodyParser.json());

// --
// Parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// --
// Parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// --
// Parses cookies and puts the cookie information on req object in the middleware
app.use(cookieParser());

// --
// Override with the X-HTTP-Method-Override header in the request.
// Simulate DELETE and PUT (express4)
app.use(methodOverride('X-HTTP-Method-Override'));


require('./routes/authenticate')(app);

// --
// Catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// --
// Catch unauthorised errors
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});

// --
// Development error handler
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// --
// Production error handler
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// EXPOSE APP ===============================================
module.exports = app;