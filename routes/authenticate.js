// ==================
// Home Routes Config
// ==================
module.exports = function(app) {

    var passport = require('passport');
    var mongoose = require('mongoose');
    var bcrypt = require('bcrypt');

    var LocalStrategy = require('passport-local').Strategy;
    var TwitterStrategy  = require('passport-twitter').Strategy;
    var FacebookStrategy = require('passport-facebook').Strategy;

    var User = require('../models/user');

    var session = require('express-session');
    mongoose.Promise = global.Promise;
    var MongoStore = require('connect-mongo')(session);
    var configAuth = require('../config/socialauth');

    var SparkPost = require('sparkpost');
    var client = new SparkPost('0d5d6fbb1e3ad0247b31d9eeee69aa6d3a4b5733');

    var async = require('async');
    var crypto = require('crypto');
    var flash = require('express-flash');

    app.use(session({
        store: new MongoStore({
            url: 'mongodb://localhost:27017/ranpariya'
         }),
        secret: 'ranpariya',
        resave:true,
        saveUninitialized:true
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    // Local Strategy
    passport.use(new LocalStrategy(function(username, password, done) {
        User.getUserByUsername(username, function(err, user){
            if(err) throw err;
            if (!user) {
                return done(null, false, {message: 'Unknown user'});
            } 

            User.comparePassword(password, user.password, function(err, isMatch){
                if(err) throw err;
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, {message: 'Invalid password'});
                }
            });
        });
    }));

    // Facebook Strategy
    passport.use(new FacebookStrategy({
        clientID       : configAuth.facebookAuth.clientID,
        clientSecret   : configAuth.facebookAuth.clientSecret,
        callbackURL    : configAuth.facebookAuth.callbackURL,
        profileFields: ['id', 'email', 'gender', 'displayName', 'name'],
        enableProof: true
    },function(accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
            User.findOne({ 'username' : profile.emails[0].value }, function (err, user) {
                if (err) return done(err);
                if (user) {
                    done(null, user);
                } else {
                    var newUser = new User();
                    newUser.displayName             = profile.displayName;
                    newUser.username                = profile.emails[0].value;
                    newUser.password                = profile.emails[0].value;
                    newUser.facebook.displayName    = profile.displayName;
                    newUser.facebook.email          = profile.emails[0].value;
                    newUser.facebook.id             = profile.id;
                    newUser.facebook.token          = accessToken;

                    User.crateUser(newUser, function(err, user){
                        if (err)
                            throw err;
                        return done(null, user);
                    });
                }
            });
        });
    }));

    // Twitter Strategy
    passport.use(new TwitterStrategy({
        consumerKey     : configAuth.twitterAuth.consumerKey,
        consumerSecret  : configAuth.twitterAuth.consumerSecret,
        callbackURL     : configAuth.twitterAuth.callbackURL
    },
    function(token, tokenSecret, profile, done) {
        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Twitter
        process.nextTick(function() {
            User.findOne({ 'username' : profile.username }, function(err, user) {
                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);
                // if the user is found then log them in
                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    // if there is no user, create them
                    var newUser = new User();
    
                    // set all of the user data that we need
                    newUser.username            = profile.username;
                    newUser.displayName         = profile.displayName;
                    newUser.password            = profile.username;
                    newUser.twitter.id          = profile.id;
                    newUser.twitter.token       = token;
                    newUser.twitter.username    = profile.username;
                    newUser.twitter.displayName = profile.displayName;
                    
                    User.crateUser(newUser, function(err, user){
                        if (err)
                            throw err;
                        return done(null, user);
                    });
                }
            });
        });
    }));

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
             done(err, user);
        });
    });

    // route for local authentication and login
    app.post('/login', passport.authenticate('local'),function(req, res){
        res.json(req.user);
    });

    // route for facebook authentication and login
    app.get('/facebook', passport.authenticate('facebook', {scope:'email'}));

    // handle the callback after facebook has authenticated the user
    app.get('/facebook/callback', passport.authenticate('facebook', {
        successRedirect : '/success',
        failureRedirect : '/'
    }));

    // route for twitter authentication and login
    app.get('/twitter', passport.authenticate('twitter'));

    // handle the callback after twitter has authenticated the user
    app.get('/twitter/callback', passport.authenticate('twitter', {
        successRedirect : '/success',
        failureRedirect : '/'
    }));

    app.get('/success', function(req, res){
        res.cookie('user', req.user);
        res.redirect('/#/dashboard/main-dashboard');
    });

    app.post('/signup',function(req,res){

        var username = req.body.email;
        var displayName = req.body.firstname;
        var firstname = req.body.firstname;
        var lastname = req.body.lastname;
        var email = req.body.email;
        var password = req.body.password;

        req.checkBody("firstname", "firstname is required").notEmpty();
        req.checkBody("lastname", "lastname is required").notEmpty();
        req.checkBody("email", "email is required").isEmail();
        req.checkBody("password", "password is required").notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            res.status(401).send(errors);
        } else {

            var newUser = new User({
                username: username,
                displayName: displayName,
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password  
            });

            User.crateUser(newUser, function(err, user){
                if (err) {
                    res.status(401).send(err);
                }else{
                    res.status(200).send(user);
                }
            });
        }
    });

    app.post('/lock-screen',function(req,res){
        var password = req.body.password;
        User.comparePassword(password, req.user.password, function(err, isMatch){
            if(err) throw err;
            if (isMatch) {
                res.status(200).send(req.user);
            } else {
                res.status(401).send(err);
            }
        });
    });

    app.post('/logout',function(req,res){
        req.logout();
        req.session.destroy();
        res.clearCookie('user');
        res.status(200).send({status: "OK!"});
    });

    app.post('/forgot-password',function(req,res, next){
        async.waterfall([ function(done) {
            crypto.randomBytes(20, function(err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function(token, done) {
            User.findOne({ email: req.body.email }, function(err, user) {
                if (!user) {
                    alert('error', 'No account with that email address exists.');
                    return res.redirect('#/pages/forgot-password');
                }
                user.resetPasswordToken = token;

                user.save(function(err) {
                    done(err, token, user);
                });
            });
        },
        function(token, user, done) {
            client.transmissions.send({
                options: {
                  sandbox: true
                },
                content: {
                  from: 'Ranpariya Lab<testing@sparkpostbox.com>',
                  subject: 'Password Reset!',
                  html: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                    'http://' + req.headers.host + '/#/reset/' + token + '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n'
                },
                recipients: [
                  {address: user.email}
                ]
              });
        }], function(err) {
            if (err) return next(err);
            res.redirect('/#/pages/forgot-password');
        });
    });

    app.post('/reset/:token', function(req, res) {
        User.findOne({ resetPasswordToken: req.params.token }, function(err, user) {
            if (!user) {
                return res.redirect('back');
            }

            var password = req.body.password;
            var confirm = req.body.confirm;

            req.checkBody("password", "password is required").notEmpty();
            req.checkBody("confirm", "password is required").equals(req.body.password);

            var errors = req.validationErrors();
            if (errors) {
                res.status(401).send(errors);
            } else {
                user.password = bcrypt.hashSync(password, 10);
                user.resetPasswordToken = undefined;

                user.save(function(err) {
                    if (err) {
                        res.status(401).send(err);
                    }else{
                        res.status(200).send(user);
                    }
                });
            }
        });  
    });
};