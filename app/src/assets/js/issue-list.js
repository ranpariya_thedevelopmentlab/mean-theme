'use strict';
/*******************************************************
 ** issue-list.js
 ** 
 ** Javascript for Navbar config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.issueList = function() {

  // --
  // Peity chart
  $(".peity-pie").peity("pie",  {
    fill: ["#ED174C", "#ccc"]
  });
};