'use strict';
/*******************************************************
 ** user-edit.js
 ** 
 ** Javascript for Navbar config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.userEdit = function() {
  $('#phone-no .inputmask').inputmask({ mask: '(999)999-9999' });

  // Square
  $('.square input').iCheck({
    radioClass: 'iradio_square-blue'
  });
};