'use strict';
/*******************************************************
 ** animate-css.js
 ** 
 ** Javascript for Navbar config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.animateCSS = function() {
  $('.animated_effect').click( function(){
    $("html,body").animate({ scrollTop: 0 });
    $('#animate-effect').removeAttr('class').attr('class', '');
    var animation = $(this).attr("data-animation");
    setTimeout(function(){
      $('#animate-effect').addClass('animated');
      $('#animate-effect').addClass(animation);
    },500);
  });
};