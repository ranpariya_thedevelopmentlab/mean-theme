'use strict';
/*******************************************************
 ** navbar.js
 ** 
 ** Javascript for Navbar config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.rightSidebar = function() {

  /*************
   * Scrollbars
   *************/
  $(".right-sidebar").mCustomScrollbar({
    autoHideScrollbar: true,
    mouseWheelPixels: 100,
    scrollInertia: 550,
    theme: "minimal-dark",
    verticalScroll: true
  });

  /*************************************
   * Left sidebar Collapsed(sidebar-v2)
   *************************************/
  $('.collapsed_sidebar_checkbox').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked) {
      $('.wrapper').addClass('sidebar-collapse');
      if($('.wrapper').hasClass('sidebar-collapse')) {
        $('.logo').removeClass('d-inline-block');
        $('.logo').addClass('d-none');
      } 
    } else {
      $('.wrapper').removeClass('sidebar-collapse');
      $('.logo').removeClass('d-none');
      $('.logo').addClass('d-inline-block');

    }
  });

  /**************************
   * Right sidebar collapsed
   **************************/
  $('.sidebar-right-sidebar').on("click", function(){
    $('.right-sidebar').toggleClass('right-sidebar-toggle');
  });
  $('.right-sidebar-collapsed').each(function() {
    $(this).on("click", function(){
      $('.chat-sidebar-chat').addClass('chat-sidebar-chat-show');
      $('#chat .input-group').hide(800);
    });
  });
  $('.chat-content-hide').on("click", function(){
    $('.chat-sidebar-chat').removeClass('chat-sidebar-chat-show');
    $('#chat .input-group').show(800);
  });
  $('.right-sidebar .nav li a').each(function() {
    $(this).on("click", function(){
      $('.chat-sidebar-chat').removeClass('chat-sidebar-chat-show');
      $('#chat .input-group').show(800);
    });
  });

  /*******************************
   * Right sidebar collapsed chat
   *******************************/
  $(".right-sidebar .nav-tabs li a").each(function() {
    $(this).on("click", function(){
      $('.chat-sidebar-chat').removeClass('chat-sidebar-chat-show');
      $('#chat .input-group').show(800);
    });
  });

  /*****************************
   * Right sidebar setting menu
   *****************************/
  $('.notifications').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  })
  $('.API-access').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  })
  $('.auto-updates').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  })
  $('.online-status').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  })
  $('.settings-responsive').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  })
  $('.activity').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  })
  $('.statisticts').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  })
};