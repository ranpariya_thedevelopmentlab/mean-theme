'use strict';
/******************************************************
 ** slider.js
 ** 
 ** Javascript for Slider Page.
 ** Developer: Ranpariya - The Development Lab - India
 ******************************************************/
this.slider = function() {

  /* Basic Slider */
  $(".basic-simple-slider").ionRangeSlider();
  $(".basic-range-slider").ionRangeSlider({
    type: "double",
    grid: true,
    min: 0,
    max: 1000,
    from: 200,
    to: 800,
    prefix: "$"
  });
  $(".prefixes-slider").ionRangeSlider({
    type: "single",
    grid: true,
    min: 0,
    max: 10000,
    from: 1000,
    prefix: "$"
  });
  $(".postfixes-slider").ionRangeSlider({
    type: "single",
    grid: true,
    min: -90,
    max: 90,
    from: 0,
    postfix: "°"
  });

  /* Advance  slider */
  $(".advance-slider-1").ionRangeSlider({
    type: "double",
    min: 1000000,
    max: 2000000,
    grid: true,
    force_edges: true
  });
  $(".advance-slider-2").ionRangeSlider({
    type: "double",
    min: 0,
    max: 10000,
    step: 500,
    grid: true,
    grid_snap: true
  });

  $(".disable-slider").ionRangeSlider({
    min: 0,
    max: 100,
    from: 30,
    disable: true
  });

};