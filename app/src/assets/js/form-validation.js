'use strict';
/*****************************************************
 ** form-validation.js
 ** 
 ** File contains custom scripts for RTDL-Admin form-validation
 ** Developer: Ranpariya - The Development Lab - India
 *****************************************************/
this.formValidation = function() {
  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
};