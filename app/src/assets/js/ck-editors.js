'use strict';
/**********************************************************
 ** ck-editors.js
 ** 
 ** File contains custom scripts for RTDL-Admin CK-Editors
 ** Developer: Ranpariya - The Development Lab - India
 **********************************************************/
this.ckEditor = function() {
  // --
  // Ck editor
  CKEDITOR.replace('ck-editor');
};