'use strict';
/*******************************************************
 ** components.js
 ** 
 ** Javascript for theme's small-small components like
 ** Raised Button, Pace (Page Loader), Scroll Top etc..
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.components = function() {

  /******************
   * Animated button
   ******************/
  var ink, d, x, y;
  $('.btn-raised').on("click", function(e){
    if($(this).find(".btn-ink").length === 0) {
      $(this).prepend("<span class='btn-ink'></span>");
    }
    ink = $(this).find(".btn-ink");
    ink.removeClass("btn-animate");
    if(!ink.height() && !ink.width()) {
      d = Math.max($(this).outerWidth(), $(this).outerHeight());
      ink.css({height: d, width: d});
    }
    x = e.pageX - $(this).offset().left - ink.width()/2;
    y = e.pageY - $(this).offset().top - ink.height()/2;
    ink.css({top: y+'px', left: x+'px'}).addClass("btn-animate");
  });

  /*******************
   * Active nav pills
   *******************/
  $('.list-group li').on("click", function(){
    $(".list-group li").removeClass("active");
    $(this).addClass("active");
  });
};