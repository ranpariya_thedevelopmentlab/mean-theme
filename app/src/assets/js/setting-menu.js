'use strict';
/*****************************************************
 ** setting-menu.js
 ** 
 ** Javascript for Theme Setting Menu
 ** NOTE: Not usable in real work. Developed just for
 ** demonstration purpose.
 ** Developer: Ranpariya - The Development Lab - India
 *****************************************************/
this.settingMenu = function() {

  /***************
   * Boxed Layout
   ***************/
  $('.box_checkbox').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked) {
      $('body').addClass('boxed-layout');
    } else {
      $('body').removeClass('boxed-layout');
    }
  });

  /***************
   * Fixed Header
   ***************/
  $('.navbar_checkbox').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked) {
      $('.header').addClass('fixed-navbar');
    } else {
      $('.header').removeClass('fixed-navbar');
    }
  });

  /***************
   * Fixed Footer
   ***************/
  $('.footer_checkbox').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked) {
      $('.footer').addClass('fixed');
    } else {
      $('.footer').removeClass('fixed');
    }
  });

  /****************
   * Fixed Sidebar
   ****************/
  $('.sidebar_checkbox').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked) {
      $('.header').addClass('fixed-navbar');
      $('.sidebar-v1, .sidebar-v2').addClass('fixed-sidebar');
      $('.navbar_checkbox').parent().addClass('checked disabled');
      $('.navbar_checkbox').attr('disabled', true);
    } else {
      $('.header').removeClass('fixed-navbar');
      $('.sidebar-v1, .sidebar-v2').removeClass('fixed-sidebar');
      $('.navbar_checkbox').parent().removeClass('checked disabled');
      $('.navbar_checkbox').attr('disabled', false);
    }
  });

  /********************
   * Theme Setting Box
   ********************/
  $('.theme-setting-icon').on("click", function(){
    $('.theme-setting-menu').toggleClass('theme-setting-menu-toggle');
  });
  $('.color-schemes a').each(function() {
    $(this).on("click", function(){
      var themes = ['strawberry-theme', 'almond-theme', 'lime-theme', 'fresh-apple-theme', 'orange-theme', 'blueberry-theme', 'teal-theme', 'elderberry-theme', 'strawberry-light-theme', 'almond-light-theme', 'lime-light-theme', 'fresh-apple-light-theme', 'orange-light-theme', 'blueberry-light-theme', 'teal-light-theme', 'elderberry-light-theme'];
      themes.forEach(function(theme) {
        $('body').removeClass(theme);
      });
      $('body').addClass($(this).data('theme'));
    });
  });
};