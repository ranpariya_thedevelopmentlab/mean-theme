'use strict';
/*******************************************************
 ** navbar-v2.js
 ** 
 ** Javascript for Navbar config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.navbarV2 = function() {

  /*********************
   * Expand Full Screen
   *********************/
  // Fullscreen Layout
  $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);
  if (!screenfull.enabled) {
    return false;
  }
  $('.expandFullscreen').click(function () {
    screenfull.toggle($('#container')[0]);
  });
  
  function fullscreenchange() {
    var elem = screenfull.element;
    $('#status').text('Is fullscreen: ' + screenfull.isFullscreen);
    if (elem) {
      $('#element').text('Element: ' + elem.localName + (elem.id ? '#' + elem.id : ''));
    }
    if (!screenfull.isFullscreen) {
      $('#external-iframe').remove();
      document.body.style.overflow = 'auto';
    }
  }
  screenfull.on('change', fullscreenchange);
  // Set the initial values
  fullscreenchange();
};