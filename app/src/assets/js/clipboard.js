'use strict';
/*******************************************************
 ** clipboard.js
 ** 
 ** Javascript for Navbar config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.clipboard = function() {
  //Clipboard button
  new Clipboard('.btn');
  // Tooltip
  $('#cut_copy_tooltip [data-toggle="tooltip"]').tooltip()
};