'use strict';
/*****************************************************
 ** rating.js
 ** 
 ** Javascript for Star Rating.
 ** Developer: Ranpariya - The Development Lab - India
 *****************************************************/
/*********
 * Rating
 *********/
this.rating = function() {

  // Basic
  $(".basic-example").rateYo({
    rating: 3
  });

  // Read Only
  $(".read-only").rateYo({
    rating: 1
  });

  // Right to Left Rating
  $(".rtl-rating").rateYo({
    rating: 2
  });

  // Space between Star rating
  $(".space-rating").rateYo({
    rating: 4.6
  });

  // Full and Half Star rating
  $(".star-rating").rateYo({
    rating: 1.7
  });

  // Custom star width set
  $(".custom-star-width").rateYo({
    rating: 4
  });

  // Rated background color
  $(".rated-bg-color").rateYo({
    rating: 4.5
  });

  // Unrated background color
  $(".unrated-bg-color").rateYo({
    rating: 2
  });

  // Number Of Star
  $(".no-of-star").rateYo({
    rating: 4
  });

  // Multi Color
  $(".multi-color").rateYo({
    rating    : 2.7,
    spacing   : "5px",
    multiColor: {
      "startColor": "#FF0000", //RED
      "endColor"  : "#00FF00"  //GREEN
    }
  });

  // Get Rating
  $(".get-rating").rateYo({
    rating : 2.5
  });
  $("#getRating").click(function () {
    var rating = $(".get-rating").rateYo("rating");
    window.alert("You select " + rating + " Rating!");
  });

  // Destroy Rating
  $(".destroy-rating").rateYo({
    rating : 3.4
  });
  $("#destroy").click(function () {
    $(".destroy-rating").rateYo("destroy");
  });
  $("#initialize").click(function () {
    $(".destroy-rating").rateYo();
  });
};