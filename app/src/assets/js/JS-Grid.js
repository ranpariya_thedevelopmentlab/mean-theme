'use strict';
/*****************************************************
 ** JS-Grid.js
 ** 
 ** File contains custom scripts for RTDL-Admin JS-Grid
 ** Developer: Ranpariya - The Development Lab - India
 *****************************************************/
this.JSGrid = function() {
  setTimeout(function(){
    $("#jsGrid").jsGrid({
      width: "100%",

      filtering: true,
      editing: true,
      sorting: true,
      paging: true,
      autoload: true,

      controller: db,

      fields: [
        { name: "Name", type: "text", width: 150 },
        { name: "Age", type: "number", width: 50 },
        { name: "Address", type: "text", width: 200 },
        { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
        { type: "control" }
      ]
    });
  },1000);
};