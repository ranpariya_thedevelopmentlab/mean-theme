'use strict';
/*******************************************************
 ** scrollspy.js
 ** 
 ** Javascript for Navbar config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.scrollspy = function() {
  /************
   * Scrollspy
   ************/
  $('.scrollspy-cn').scrollspy({ target: '.scrollspy', offset: 0 })
  function setContentHeight(){
    $('.scrollspy-cn').height($(window).height() - 300);
  }
  setContentHeight();
  $(window).resize(function(){
    setContentHeight();
  });
};