'use strict';
/*****************************************************
 ** main-dashboard.js
 ** 
 ** Javascript Configs for Main Dashboard.
 ** Developer: Ranpariya - The Development Lab - India
 *****************************************************/
this.mainDashboard = function() {

  /******************
   * Remove card
   ******************/
  $('.remove-card .fa-trash-o').on("click", function(){
    $(this).parents(".card").parent().remove();
  });

  /********
   * Toastr
   ********/
  Command: toastr["success"]("Welcome To Ranpariya Theme");

  /*******************
   * Daily-feed Charts
   *******************/
  $("span.bar").peity("bar", {fill: ["#ccc", "#ED174C"]})
  $("span.pie").peity("pie", {fill: ["#ccc", "#ED174C"]})
  $("span.donut").peity("donut", {fill: ["#ccc", "#ED174C"]})

  /************
   * Todo lists
   ************/
  $('#square input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked == true) {
      $(this).parent().parent().addClass('active');
    } else {
      $(this).parent().parent().removeClass('active');
    }
  });

  /*********************
   * Header Peity Charts
   *********************/

  var barChart1, barChart2, barChart3;
  $('.color-schemes a').each(function() {
    $('.color-schemes a').on("click", function(){
      barChart1 = $(".barChart1").peity("bar", {fill: [$(this).data('themecolor'), "#909190"], width:70, height: 40, })
      barChart2 = $(".barChart2").peity("bar", {fill: [$(this).data('themecolor'), "#909190"], width:70, height: 40, })
      barChart3 = $(".barChart3").peity("bar", {fill: [$(this).data('themecolor'), "#909190"], width:70, height: 40, })
    });
  });
  var barChart1 = $(".barChart1").peity("bar", {fill: ["#ED174C", "#CCCCCC"], width:70, height: 40})
  setInterval(function() {
    var random = Math.round(Math.random() * 10)
    var values = barChart1.text().split(",")
    values.shift()
    values.push(random)

    barChart1
      .text(values.join(","))
      .change()
  }, 1000);

  var barChart2 = $(".barChart2").peity("bar", {fill: ["#ED174C", "#CCCCCC"], width:70, height: 40})
  setInterval(function() {
    var random = Math.round(Math.random() * 10)
    var values = barChart2.text().split(",")
    values.shift()
    values.push(random)

    barChart2
      .text(values.join(","))
      .change()
  }, 1000);

  var barChart3 = $(".barChart3").peity("bar", {fill: ["#ED174C", "#CCCCCC"], width:70, height: 40})
  setInterval(function() {
    var random = Math.round(Math.random() * 10)
    var values = barChart3.text().split(",")
    values.shift()
    values.push(random)

    barChart3
      .text(values.join(","))
      .change()
  }, 1000);

  /*****************
   * Sparkline Chart
   *****************/
  $('.todaySelling').sparkline([ 7,4,6,3,8,2,5 ], {type: 'line', width: '60',height: '20',lineColor: '#007bff', tooltipChartTitle: "Today's Selling", fillColor: "transparent"} );
  $('.weeklySelling').sparkline([ 8,2,5,3,1,7,5 ], {type: 'line', width: '60',height: '20',lineColor: '#17a2b8', tooltipChartTitle: "Weekly Selling", fillColor: "transparent"} );
  $('.yearlySelling').sparkline([ 3,7,2,8,6,5,7 ], {type: 'line', width: '60',height: '20',lineColor: '#ffc107', tooltipChartTitle: "Monthly Selling", fillColor: "transparent"} );
  $('.totalSelling').sparkline([ 4,8,2,8,6,9,4 ], {type: 'line', width: '60',height: '20',lineColor: '#ED174C', tooltipChartTitle: "Total Selling", fillColor: "transparent"} );
  var sparklineCharts = function(){
    $('.visitorsSparkline').sparkline([ 7,4,6,3,8,2,5 ], {type: 'line', width: '100%',height: '50',lineColor: '#17a2b8', tooltipChartTitle: "Visitors", fillColor: "transparent"} );
    $('.weeklySales').sparkline([ 4,7,2,8,6,9,2 ], {type: 'line', width: '140',height: '50',lineColor: '#ED174C', tooltipChartTitle: "Weekly Selling", fillColor: "transparent"} );
  };
  var sparkResize;
  $(window).resize(function(e) {
    clearTimeout(sparkResize);
    sparkResize = setTimeout(sparklineCharts, 300);
  });
  sparklineCharts();

  /*******
   * Knob
   *******/
  $(".knob-simple").knob({
    'format' : function (value) {
      return value + '%';
    }
  });

  /***********
   * AM Charts
   ***********/
  var chart = AmCharts.makeChart("combineChart", {
    "type": "serial",
    "theme": "light",
    "dataDateFormat": "YYYY-MM-DD",
    "precision": 2,
    "valueAxes": [{
      "id": "v1",
      "axisAlpha": 0,
      "position": "left",
      "autoGridCount": false,
      "gridThickness": 0,
      "labelFunction": function(value) {
        return "$" + Math.round(value) + "M";
        }
      }, {
      "id": "v2",
      "axisAlpha": 0,
      "gridAlpha": 0,
      "position": "right",
      "autoGridCount": false,
      "gridThickness": 0,
    }],
    "graphs": [{
      "id": "g3",
      "valueAxis": "v1",
      "lineColor": "#EB849C",
      "fillColors": "#EB849C",
      "fillAlphas": 1,
      "type": "column",
      "title": "Today Sales",
      "valueField": "sales2",
      "clustered": false,
      "columnWidth": 0.5,
      "legendValueText": "$[[value]]M",
      "balloonText": "[[title]]<br /><b style='font-size: 130%'>$[[value]]M</b>"
    }, {
      "id": "g4",
      "valueAxis": "v1",
      "lineColor": "#ED174C",
      "fillColors": "#ED174C",
      "fillAlphas": 1,
      "type": "column",
      "title": "Target Sales",
      "valueField": "sales1",
      "clustered": false,
      "columnWidth": 0.3,
      "legendValueText": "$[[value]]M",
      "balloonText": "[[title]]<br /><b style='font-size: 130%'>$[[value]]M</b>"
    }, {
      "id": "g1",
      "valueAxis": "v2",
      "bullet": "round",
      "bulletBorderAlpha": 1,
      "bulletColor": "#6E6E6E",
      "bulletSize": 5,
      "hideBulletsCount": 50,
      "lineThickness": 2,
      "lineColor": "#6E6E6E",
      "type": "smoothedLine",
      "title": "Market Days",
      "useLineColorForBulletBorder": true,
      "valueField": "market1",
      "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
    }, {
      "id": "g2",
      "valueAxis": "v2",
      "bullet": "round",
      "bulletBorderAlpha": 1,
      "bulletColor": "#1e1e1e",
      "bulletSize": 5,
      "hideBulletsCount": 50,
      "lineThickness": 2,
      "lineColor": "#1e1e1e",
      "type": "smoothedLine",
      "dashLength": 5,
      "title": "Market Days ALL",
      "useLineColorForBulletBorder": true,
      "valueField": "market2",
      "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
    }],
    "chartScrollbar": {
      enabled: false
    },
    "chartCursor": {
      "pan": true,
      "valueLineEnabled": true,
      "valueLineBalloonEnabled": true,
      "cursorAlpha": 0,
      "valueLineAlpha": 0.2
    },
    "categoryField": "date",
      "categoryAxis": {
      "parseDates": true,
      "dashLength": 1,
      "minorGridEnabled": false,
      "gridThickness": 0
    },
    "legend": {
      "enabled": false,
      "useGraphSettings": true,
      "position": "top"
    },
    "balloon": {
      "borderThickness": 1,
      "shadowAlpha": 0
    },
    "export": {
      "enabled": true
    },
    "dataProvider": [{
      "date": "2017-10-01",
      "market1": 71,
      "market2": 75,
      "sales1": 5,
      "sales2": 8
    }, {
      "date": "2017-10-02",
      "market1": 74,
      "market2": 78,
      "sales1": 4,
      "sales2": 6
    }, {
      "date": "2017-10-03",
      "market1": 78,
      "market2": 88,
      "sales1": 5,
      "sales2": 2
    }, {
      "date": "2017-10-04",
      "market1": 85,
      "market2": 89,
      "sales1": 8,
      "sales2": 9
    }, {
      "date": "2017-10-05",
      "market1": 82,
      "market2": 89,
      "sales1": 9,
      "sales2": 6
    }, {
      "date": "2017-10-06",
      "market1": 83,
      "market2": 85,
      "sales1": 3,
      "sales2": 5
    }, {
      "date": "2017-10-07",
      "market1": 88,
      "market2": 92,
      "sales1": 5,
      "sales2": 7
    }, {
      "date": "2017-10-08",
      "market1": 85,
      "market2": 90,
      "sales1": 7,
      "sales2": 6
    }, {
      "date": "2017-10-09",
      "market1": 85,
      "market2": 91,
      "sales1": 9,
      "sales2": 5
    }, {
      "date": "2017-10-10",
      "market1": 80,
      "market2": 84,
      "sales1": 5,
      "sales2": 8
    }, {
      "date": "2017-10-11",
      "market1": 87,
      "market2": 92,
      "sales1": 4,
      "sales2": 8
    }, {
      "date": "2017-10-12",
      "market1": 84,
      "market2": 87,
      "sales1": 3,
      "sales2": 4
    }, {
      "date": "2017-10-13",
      "market1": 83,
      "market2": 88,
      "sales1": 5,
      "sales2": 7
    }, {
      "date": "2017-10-14",
      "market1": 79,
      "market2": 87,
      "sales1": 5,
      "sales2": 8
    }, {
      "date": "2017-10-15",
      "market1": 81,
      "market2": 85,
      "sales1": 4,
      "sales2": 7
    },{
      "date": "2017-10-16",
      "market1": 77,
      "market2": 79,
      "sales1": 5,
      "sales2": 8
    }, {
      "date": "2017-10-17",
      "market1": 84,
      "market2": 78,
      "sales1": 4,
      "sales2": 6
    }, {
      "date": "2017-10-18",
      "market1": 78,
      "market2": 88,
      "sales1": 5,
      "sales2": 2
    }, {
      "date": "2017-10-19",
      "market1": 85,
      "market2": 81,
      "sales1": 8,
      "sales2": 9
    }, {
      "date": "2017-10-20",
      "market1": 82,
      "market2": 89,
      "sales1": 9,
      "sales2": 6
    }, {
      "date": "2017-10-21",
      "market1": 83,
      "market2": 85,
      "sales1": 3,
      "sales2": 5
    }, {
      "date": "2017-10-22",
      "market1": 88,
      "market2": 92,
      "sales1": 5,
      "sales2": 7
    }, {
      "date": "2017-10-23",
      "market1": 85,
      "market2": 90,
      "sales1": 7,
      "sales2": 6
    }, {
      "date": "2017-10-24",
      "market1": 85,
      "market2": 91,
      "sales1": 9,
      "sales2": 5
    }, {
      "date": "2017-10-25",
      "market1": 80,
      "market2": 84,
      "sales1": 5,
      "sales2": 8
    }, {
      "date": "2017-10-26",
      "market1": 87,
      "market2": 92,
      "sales1": 4,
      "sales2": 8
    }, {
      "date": "2017-10-27",
      "market1": 84,
      "market2": 87,
      "sales1": 3,
      "sales2": 4
    }, {
      "date": "2017-10-28",
      "market1": 83,
      "market2": 88,
      "sales1": 5,
      "sales2": 7
    }, {
      "date": "2017-10-29",
      "market1": 84,
      "market2": 87,
      "sales1": 5,
      "sales2": 8
    }, {
      "date": "2017-10-30",
      "market1": 81,
      "market2": 85,
      "sales1": 4,
      "sales2": 7
    }]
  });

  /**************
   * Sales Charts
   **************/
  var chart = AmCharts.makeChart("salesChart", {
    "type": "serial",
    "theme": "light",
    "dataDateFormat": "YYYY-MM-DD",
    "graphs": [{
      "lineColor": "#ED174C",
      "fillColors": "#ED174C",
      "fillAlphas": 1,
      "type": "column",
      "valueField": "sales",
      "balloonText": "<b>[[category]]: [[value]]</b>",
    }],
    "chartScrollbar": {
      enabled: false
    },
    "categoryField": "date",
    "categoryAxis": {
      "gridThickness": 0,
      "parseDates": true
    },
    "valueAxes": [{
      "axisAlpha": 0
    }],
    "legend": {
      "enabled": false
    },
    "export": {
      "enabled": true
    },
    "dataProvider": [{
      "date": "2017-12-01",
      "sales": 5
    }, {
      "date": "2017-12-02",
      "sales": 4
    }, {
      "date": "2017-12-03",
      "sales": 5
    }, {
      "date": "2017-12-04",
      "sales": 8
    }, {
      "date": "2017-12-05",
      "sales": 9
    }, {
      "date": "2017-12-06",
      "sales": 3
    }, {
      "date": "2017-12-07",
      "sales": 5
    }, {
      "date": "2017-12-08",
      "sales": 7
    }, {
      "date": "2017-12-09",
      "sales": 9
    }, {
      "date": "2017-12-10",
      "sales": 5
    }, {
      "date": "2017-12-11",
      "sales": 4
    }, {
      "date": "2017-12-12",
      "sales": 3
    }, {
      "date": "2017-12-13",
      "sales": 5
    }, {
      "date": "2017-12-14",
      "sales": 5
    }, {
      "date": "2017-12-15",
      "sales": 4
    },{
      "date": "2017-12-16",
      "sales": 5
    }, {
      "date": "2017-12-17",
      "sales": 4
    }, {
      "date": "2017-12-18",
      "sales": 5
    }, {
      "date": "2017-12-19",
      "sales": 8
    }, {
      "date": "2017-12-20",
      "sales": 9
    }, {
      "date": "2017-12-21",
      "sales": 3
    }, {
      "date": "2017-12-22",
      "sales": 5
    }, {
      "date": "2017-12-23",
      "sales": 7
    }, {
      "date": "2017-12-24",
      "sales": 9
    }, {
      "date": "2017-12-25",
      "sales": 5
    }, {
      "date": "2017-12-26",
      "sales": 4
    }, {
      "date": "2017-12-27",
      "sales": 3
    }, {
      "date": "2017-12-28",
      "sales": 5
    }, {
      "date": "2017-12-29",
      "sales": 5
    }, {
      "date": "2017-12-30",
      "sales": 4
    }, {
      "date": "2017-12-31",
      "sales": 8
    }]
  });

  /*********************
   * Chartist Line Chart
   *********************/
  $('#myTab a[href="#revenue"]').on('click', function (e) {
    var data = {
        labels: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        series: [
          { "name": "2016", "data": [7, 4, 6, 5, 10, 3, 5, 7, 4, 8, 5, 10] },
          { "name": "2017", "data": [5, 6, 7, 9, 12, 5, 3, 4, 7, 5, 10, 10] },
        ]
      },
      chart = new Chartist.Line('.revenue', data, {
        low: 0,
        showArea: true,
        fullWidth: true,
        height: "200",
        axisY: {
          // showGrid: false,
          showLabel: false,
          offset: 0
        },
      });

    chart.on('draw', function(data) {
      if(data.type === 'line' || data.type === 'area') {
        data.element.animate({
          d: {
            begin: 2000 * data.index,
            dur: 2000,
            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint
          }
        });
      }
    });
  })

  /****************
   * Visitors Chart
   ****************/
  setTimeout(function() {
    var visitorsChart = new Chartist.Line('.visitorsChart', {
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        series: [
          { "name": "2016", "data": [68, 50, 30, 55, 45, 65, 60, 105, 80, 110, 120, 150] },
          { "name": "2017", "data": [168, 135, 107, 155, 140, 147, 130, 180, 160, 175, 165, 200] },
        ]
    }, {
        showArea: true,
        fullWidth: true,
        height: "200",
        lineSmooth: Chartist.Interpolation.none(),
        axisX: {
            showGrid: false,
        },
        axisY: {
            low: 0,
            scaleMinSpace: 30,
        }
    });

    visitorsChart.on('created', function (data) {
      var defs = data.svg.elem('defs');
      defs.elem('linearGradient', {
          id: 'chartist-gradient',
          x1: 0,
          y1: 1,
          x2: 0,
          y2: 0
      }).elem('stop', {
          offset: 0,
          'stop-opacity': '0.2',
          'stop-color': 'rgba(255, 255, 255, 1)'
      }).parent().elem('stop', {
          offset: 1,
          'stop-opacity': '0.2',
          'stop-color': 'rgba(255, 0, 255, 1)'
      });

      defs.elem('linearGradient', {
          id: 'chartist-gradient1',
          x1: 0,
          y1: 1,
          x2: 0,
          y2: 0
      }).elem('stop', {
          offset: 0.3,
          'stop-opacity': '0.2',
          'stop-color': 'rgba(255, 255, 255, 1)'
      }).parent().elem('stop', {
          offset: 1,
          'stop-opacity': '0.2',
          'stop-color': 'rgba(127, 0, 255, 1)'
      });
    });
    visitorsChart.on('draw', function (data) {
      var circleRadius = 4;
      if (data.type === 'point') {

          var circle = new Chartist.Svg('circle', {
              cx: data.x,
              cy: data.y,
              r: circleRadius,
              class: 'ct-point-circle'
          });
          data.element.replace(circle);
      }
      else if (data.type === 'label') {
          // adjust label position for rotation
          const dX = data.width / 2 + (30 - data.width)
          data.element.attr({ x: data.element.attr('x') - dX })
      }
    });
  }, 500);
};