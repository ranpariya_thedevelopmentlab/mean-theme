'use strict';
/*****************************************************
 ** data-analytics-dashboard.js
 ** 
 ** Javascript for Data Analytics Dashboard.
 ** Developer: Ranpariya - The Development Lab - India
 *****************************************************/
/***************************
 * Data Analytics Dashboard
 ***************************/
this.dataAnalyticsDashboard = function() {

  /*****************
   * Remove Section
   *****************/
  $('.remove-card .fa-trash-o').on("click", function(){
    $(this).parents(".card").parent().remove();
  });

  /*******
   * Knob
   *******/
  $(".data-analytics-knob").knob({
    'format' : function (value) {
      return value + '%';
    }
  });

  /*****************************
   * AM Chart(social-analytics)
   *****************************/
  var socialAnalyticsData = [
    {
      "month": "Jan",
      "facebook": 350,
      "twitter": 408
    },
    {
      "month": "Feb",
      "facebook": 450,
      "twitter": 482
    },
    {
      "month": "Mar",
      "facebook": 890,
      "twitter": 200
    },
    {
      "month": "Apr",
      "facebook": 500,
      "twitter": 379
    },
    {
      "month": "May",
      "facebook": 980,
      "twitter": 501
    },
    {
      "month": "Jun",
      "facebook": 450,
      "twitter": 900
    },
    {
      "month": "Jul",
      "facebook": 650,
      "twitter": 405
    },
    {
      "month": "Aug",
      "facebook": 400,
      "twitter": 309
    },
    {
      "month": "Sep",
      "facebook": 900,
      "twitter": 287
    },
    {
      "month": "Oct",
      "facebook": 349,
      "twitter": 485
    },
    {
      "month": "Nov",
      "facebook": 603,
      "twitter": 890
    },
    {
      "month": "Dec",
      "facebook": 200,
      "twitter": 500,
      "bulletClass": "lastBullet"
    }
  ];
  var chart = AmCharts.makeChart( "social-analytics", {
    "type": "serial",
    "theme": "light",
    "dataProvider": socialAnalyticsData,
    "addClassNames": true,
    "startDuration": 1,
    "marginLeft": 0,
    "legend": {
      "position": "top",
      "useGraphSettings": true,
      "fontSize": 12,
      "align": "center"
    },
    "categoryField": "month",
    "categoryAxis": {
      "gridCount": 50,
      "axisAlpha":0,
      "gridAlpha":0,
      "gridColor": "#FFFFFF",
      "axisColor": "#555555"
    },
    'allLabels': false,
    "valueAxes": [
      {
        "id": "a1",
        "gridAlpha": 0,
        "axisAlpha": 0
      },
      {
        "id": "a3",
        "position": "right",
        "gridAlpha": 0,
        "axisAlpha": 0
      }
    ],
    "graphs": [
      {
        "id": "g1",
        "title": "Facebook Analytics",
        "valueField": "facebook",
        "type": "column",
        "fillAlphas": 0.9,
        "valueAxis": "a1",
        "balloonText": "Facebook Analytics : [[value]]",
        "lineColor": "#3B5998",
        "alphaField": "alpha"
      },
      {
        "id": "g3",
        "title": "Twitter Analytics",
        "valueField": "twitter",
        "type": "line",
        "valueAxis": "a3",
        "balloonText": "Twitter Analytics : [[value]]",
        "lineColor": "#1C92C7",
        "lineThickness": 1,
        "bullet": "round",
        "bulletBorderColor": "#1C92C7",
        "bulletBorderThickness": 1,
        "bulletBorderAlpha": 1,
        "dashLengthField": "dashLength",
        "animationPlayed": true
      }
    ]
  });

  /**************************************
   * Sparkline Chart(Today Active Users)
   **************************************/
  var sparklineCharts = function(){
    $('.todayActiveUsers').sparkline([ 5,3,9,6,5,9,7,3,5,2 ], {type: 'line', width: '100%',height: '50',lineColor: '#ED174C', tooltipChartTitle: "Active Users", fillColor: "transparent"} );
  };
  var sparkResize;
  $(window).resize(function(e) {
    clearTimeout(sparkResize);
    sparkResize = setTimeout(sparklineCharts, 300);
  });
  sparklineCharts();

  // countup
  $(".today-users [data-from][data-to]").each(function(i,el) {
    var $el = $(el),
      sm = scrollMonitor.create(el);
      sm.fullyEnterViewport(function() {
        var opts = {
          useEasing: true,
          useGrouping: true,
          separator: ',',
          decimal: '.',
          prefix: $el.data('prefix'),
          suffix: $el.data('suffix'),
        },
        $count = $el.find('.num'),
        from = $el.data('from'),
        to = $el.data('to'),
        duration = $el.data('duration'),
        delay = 1,
        decimals = new String(to).match(/\.([0-9]+)/)?new String(to).match(/\.([0-9]+)$/)[1].length:0,
        counter = new CountUp($count.get(0),from,to,decimals,duration,opts);
        setTimeout(function() {
          counter.start();
        },delay*1000);
      sm.destroy();
    });
  });

  /********************************
   * Line Chart(Weekly User Chart)
   ********************************/
  var lineData = {
    labels: ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"],
    datasets: [
      {
        label: "weekly Users",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(75,192,192,1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: "rgba(75,192,192,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [65, 59, 80, 81, 56, 55, 60],
        spanGaps: false,
      }
    ]
  };
  var weeklyUserLineChart = new Chart(document.getElementById("weeklyUserChart"), {
    type: 'line',
    data: lineData
  });

  /***************************
   * AM-Chart(Device Progress)
   ***************************/
  var visitedDeviceChart = AmCharts.makeChart("visited-device-chart", {
    "type": "serial",
    "theme": "light",
    "legend": {
      "position": "top",
      "useGraphSettings": true,
      "fontSize": 12
    },
    "dataProvider": [
      {
        "country": "India",
        "iPhone": 430,
        "Blackberry": 610,
        "Mac": 480,
        "Android": 560
      },
      {
        "country": "Mexico",
        "iPhone": 620,
        "Blackberry": 325,
        "Mac": 360,
        "Android": 330
      },
      {
        "country": "Japan",
        "iPhone": 345,
        "Blackberry": 450,
        "Mac": 280,
        "Android": 745
      },
      {
        "country": "London",
        "iPhone": 470,
        "Blackberry": 650,
        "Mac": 432,
        "Android": 450
      },
      {
        "country": "New York",
        "iPhone": 510,
        "Blackberry": 448,
        "Mac": 289,
        "Android": 456
      },
      {
        "country": "USA",
        "iPhone": 300,
        "Blackberry": 500,
        "Mac": 281,
        "Android": 323
      },
      {
        "country": "AUS",
        "iPhone": 410,
        "Blackberry": 400,
        "Mac": 440,
        "Android": 530
      }
    ],
    "valueAxes": [{
      "stackType": "regular",
      "axisAlpha": 0.5,
      "gridAlpha": 0
    }],
    "graphs": [
      {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "title": "iPhone",
        "type": "column",
        "color": "#FFFFFF",
        "valueField": "iPhone",
        "lineColor": "#C73B0B"
      }, 
      {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "title": "Blackberry",
        "type": "column",
        "valueField": "Blackberry",
        "lineColor": "#E7A136"
      },
      {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "title": "Mac",
        "type": "column",
        "valueField": "Mac",
        "lineColor": "#978E43"
      },
      {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "title": "Android",
        "type": "column",
        "valueField": "Android",
        "lineColor": "#844204"
      }
    ],
    "rotate": true,
    "categoryField": "country",
    "categoryAxis": {
      "gridPosition": "start",
      "axisAlpha": 0,
      "gridAlpha": 0,
      "position": "left"
    },
    "export": {
      "enabled": true
    }
  });

  /**************************
   * Pie Chart(Browser Usage)
   **************************/
  var chart = AmCharts.makeChart( "browserUsageChart", {
    "type": "pie",
    "theme": "none",
    "dataProvider": [ 
      {
        "browser": "Chrome",
        "value": 400,
        "color":"#ED174C"
      },
      {
        "browser": "FireFox",
        "value": 150,
        "color":"#2C2B2B"
      },
      {
        "browser": "Safari",
        "value": 65,
        "color":"#6E6E6E"
      }
    ],
    "valueField": "value",
    "titleField": "browser",
    "labelsEnabled": false,
    "colorField": "color",
    "outlineAlpha": 0.4,
    "depth3D": 15,
    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "angle": 30,
    "export": {
      "enabled": false
    }
  });

  /**************************
   * ChartJS(Traffic Sources)
   **************************/
  var bar = document.getElementById("traffic-sources-chart"),
      barOption = {
        type: 'bar',
        data: {
          labels: ["Direct", "Organic", "Referral", "Others"],
          datasets: [{
            label: 'Site Traffic',
            data: [53, 10, 43, 50],
            backgroundColor: [
            'rgba(0, 157, 160, 1)',
            'rgba(0, 157, 160, 1)',
            'rgba(0, 157, 160, 1)',
            'rgba(0, 157, 160, 1)'
            ],
            borderColor: [
            'rgba(3, 185, 188, 1)',
            'rgba(3, 185, 188, 1)',
            'rgba(3, 185, 188, 1)',
            'rgba(3, 185, 188, 1)'
            ],
            borderWidth: 1
          }]
        },
        grid: {
          show: false
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }]
          }
        }
      };

  barOption.type = 'bar';
  var bar = new Chart(bar, barOption);

  /*******************************
  * Update Flot Chart(Server Load)
  ********************************/
  setTimeout(function() {

    var data = [],
    totalPoints = 300;

    function getRandomData() {

      if (data.length > 0)
      data = data.slice(1);

      /* Do a random walk */

      while (data.length < totalPoints) {

        var prev = data.length > 0 ? data[data.length - 1] : 50,
        y = prev + Math.random() * 10 - 5;

        if (y < 0) {
          y = 0;
        } else if (y > 99) {
          y = 99;
        }

        data.push(y);
      }

      /* Zip the generated y values with the x values */

      var res = [];
      for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
      }

      return res;
    }

    /* Set up the control widget */

    var updateInterval = 30;
    $("#updateInterval").val(updateInterval).change(function () {
      var v = $(this).val();
      if (v && !isNaN(+v)) {
        updateInterval = +v;
        if (updateInterval < 1) {
          updateInterval = 1;
        } else if (updateInterval > 2000) {
          updateInterval = 2000;
        }
        $(this).val("" + updateInterval);
      }
    });

    var updateChart = $.plot("#server-load-updateChart", [ getRandomData() ], {
      series: {
        shadowSize: 0,
        lines: {
          show: true,
          fill: true
        }
      },
      yaxis: {
        min: 0,
        max: 100
      },
      xaxis: {
        show: false
      },
      grid: {
        show: false
      },
      colors: ['#1BB198']
    });

    function update() {
      updateChart.setData([getRandomData()]);

      /* Since the axes don't change, we don't need to call plot.setupGrid() */

      updateChart.draw();
      setTimeout(update, updateInterval);
    }

    update();
  }, 1000);
};