'use strict';
/*******************************************************
 ** custom-scrollbar.js
 ** 
 ** Javascript for Navbar config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.customScroll = function() {
  /*************
   * Scrollbars
   *************/
  $(".notification-scrollbar, .mail-scrollbar").mCustomScrollbar({
    autoHideScrollbar: true,
    mouseWheelPixels: 100,
    scrollInertia: 550,
    theme: "minimal-dark",
    verticalScroll: true
  });
};