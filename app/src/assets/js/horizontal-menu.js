'use strict';
/**************************************************************
 ** horizontal-menu.js
 ** 
 ** File contains custom scripts for RTDL-Admin hprizontal-menu
 ** Developer: Ranpariya - The Development Lab - India
 **************************************************************/
this.horizontalLayout = function() {
  function horizontalMenu() {
    if ($( window ).outerWidth() > 767 ) {
      $('.horizontal-menu .dropdown').mouseenter(function(){
        $('.dropdown-menu', this).addClass("show");
      });
      $('.horizontal-menu .dropdown').mouseleave(function(){
        $('.dropdown-menu', this).removeClass("show");
      });
      $('.dropdown').removeClass("show");
      $('.dropdown-menu').removeClass("show");
      $( ".horizontal-menu .navbar-nav .nav-item" ).last().addClass("ml-auto");
    } else {
      $('.horizontal-menu .dropdown').off("mouseenter");
      $('.horizontal-menu .dropdown').off("mouseleave");
      $( ".horizontal-menu .navbar-nav .nav-item" ).last().removeClass("ml-auto");
    }
  }
  horizontalMenu();
  $(window).resize(function(){
    horizontalMenu();
  });
};