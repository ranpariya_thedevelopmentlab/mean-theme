'use strict';
/*******************************************************
 ** sidebar-component-config.js
 ** 
 ** Javascript Config for Sidebar Component Config.
 ** Developer: Ranpariya - The Development Lab - India
 *******************************************************/
this.sidebarV1 = function() {

  /*****************
   * Sidebar Toggle
   *****************/
  var _left = '0px';
  function resizeTopMenu() {
    if($('.wrapper').hasClass('toggle-sidebar')) {
      _left = '230px';
    } else {
      _left = '0px';
    }
    $('.navbar.navbar-lg .nav-block').css('padding-left', _left + ' !important');
  }
  resizeTopMenu();
  $('.menu-toggle').on("click", function(){
    $('.wrapper').toggleClass('toggle-sidebar');
  });

  /* Page wrapper content height setter. */
  function setContentHeight(){
    var _sidebarHeight = $('.sidebar-v1').height(),
      _mainWrapper = $('.main-wrapper').height() + 50,
      winH = 600,
      windowWidth = 767;
    if(document.body && document.body.offsetWidth) {
      windowWidth = document.body.offsetWidth;
      winH = document.body.offsetHeight;
    }
    if(document.compatMode=='CSS1Compat' && document.documentElement && document.documentElement.offsetWidth) {
      windowWidth = document.documentElement.offsetWidth;
      winH = document.documentElement.offsetHeight;
    }
    if(window.innerWidth && window.innerHeight) {
      windowWidth = window.innerWidth;
      winH = window.innerHeight;
    }
    winH = winH - 51;
    $('.main-wrapper').css('min-height', winH + "px");
    if(windowWidth <= 767)
      $('.sidebar-v1').css('height', $('.main-wrapper').height() + 71);
  }
  setContentHeight();
  $(window).resize(function(){
    setContentHeight();
  });

  /***************
   * Sidebar-menu
   ***************/
  var menuLinkClass = $('.sidebar-menu li .list-link'),
    subMenuClass = $('.sub-menu .sub-list'),
    animationSpeed = 200;

  menuLinkClass.on("click", function(e) {

    /* Get the clicked link and the next element */
    var thisElement = $(this),
      nextElement = thisElement.next();

    /* Check if the next element is a menu and is visible */
    if((nextElement.is('.sub-menu')) && (nextElement.hasClass('open'))) {

      /*****************
       * Close the menu
       *****************/
      nextElement.slideUp(animationSpeed, function () {
        nextElement.removeClass('open');
      });
      thisElement.removeClass("active");

    } else if((nextElement.is('.sub-menu')) && (!nextElement.hasClass('open'))) {
      var parentElement = thisElement.parents('ul').first(),
        ulElement = parentElement.find('ul:visible').slideUp(animationSpeed),
        parentLElement = thisElement.parent("li");

      /****************
       * Open the menu
       ****************/
      ulElement.removeClass('open');
      nextElement.addClass('open');
      menuLinkClass.removeClass('active');
      thisElement.addClass('active');
      nextElement.slideDown(animationSpeed, function () {
      });
    }
  });

  /***********************************
   * Left sidebar menu(Vertical Menu)
   ***********************************/
  $('.sidebar-v1 .vertical-menu li').on('click', function(e) {
    e.preventDefault();
    var _self = $(this),
      _id = _self.data('target'),
      _page = _self.data('page');
    if(_page) {
      window.location = _page;
      return false;
    }
    if(!_id)
      return false;
    $('.sidebar-v1 .vertical-menu li').each(function() {
      var _target = $(this).data('target');
      if(_target) {
        $(this).removeClass('active');
        $(_target).addClass('d-none');
      }
    });
    _self.addClass('active');
    $(_id).removeClass('d-none');
  });

  /************
   * Scrollbar
   ************/
  $(window).on("load",function() {
    $(".sidebar-v1").mCustomScrollbar({
      autoHideScrollbar: true,
      mouseWheelPixels: 100,
      scrollInertia: 550,
      theme: "minimal-dark",
      verticalScroll: true
    });
  });
  
  /**********************
   * Left sidebar charts
   **********************/
  var facebookStatistics, commentsStatistics;
  $('.color-schemes a').each(function() {
    $('.color-schemes a').on("click", function(){
      facebookStatistics = $(".facebookStatistics").peity("bar", {fill: [$(this).data('themecolor'), "#909190"], width:100, height: 30, })
      commentsStatistics = $(".commentsStatistics").peity("bar", {fill: [$(this).data('themecolor'), "#909190"], width:100, height: 30,  })
    });
  });

  facebookStatistics = $(".facebookStatistics").peity("bar", {fill: ["#ED174C", "#909190"], width:100, height: 30, })
  setInterval(function() {
    var random = Math.round(Math.random() * 10)
    var values = facebookStatistics.text().split(",")
    values.shift()
    values.push(random)
    facebookStatistics
      .text(values.join(","))
      .change()
  }, 1000);
  commentsStatistics = $(".commentsStatistics").peity("bar", {fill: ["#ED174C", "#909190"], width:100, height: 30,  })
  setInterval(function() {
    var random = Math.round(Math.random() * 10)
    var values = commentsStatistics.text().split(",")
    values.shift()
    values.push(random)
    commentsStatistics
      .text(values.join(","))
      .change()
  }, 1000);

  /***********************
   * Always Sticky Header
   ***********************/
  if(window.localStorage.getItem("always-sticky-header")) {
    $('.always-sticky-header').prop('checked', true);
    $('.header').addClass('fixed-navbar');
    $('.navbar_checkbox').parent().addClass('checked disabled');
    $('.navbar_checkbox').attr('checked', true);
    $('.navbar_checkbox').attr('disabled', true);
  }

  $('.always-sticky-header').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked == true) {
      $('.navbar_checkbox').parent().addClass('checked disabled');
      $('.navbar_checkbox').attr('checked', true);
      $('.navbar_checkbox').attr('disabled', true);

      if(typeof(Storage) !== "undefined") {
        /* Store */
        window.localStorage.setItem("always-sticky-header", true);
        $('.header').addClass('fixed-navbar');
      }
    } else {
      window.localStorage.removeItem("always-sticky-header");

      /*************************
       * Check for header class
       *************************/
      if(!$('.sidebar_checkbox').checked == false) {
        $('.header').removeClass('fixed-navbar');
      }

      if(!$('.sidebar-v1').hasClass('fixed-sidebar')) {
        $('.navbar_checkbox').parent().removeClass('checked disabled');
        $('.navbar_checkbox').attr('checked', false);
        $('.navbar_checkbox').attr('disabled', false);
      }
    }
  });

  /************************
   * Always Sticky Sidebar
   ************************/
  if(window.localStorage.getItem("always-sticky-sidebar")) {
    $('.always-sticky-sidebar').prop('checked', true);
    $('.sidebar-v1').addClass('fixed-sidebar');
    $('.header').addClass('fixed-navbar');
    $('.sidebar_checkbox').parent().addClass('checked disabled');
    $('.always-sticky-header').parent().addClass('checked disabled');
    $('.navbar_checkbox').parent().addClass('checked disabled');
    $('.sidebar_checkbox').attr('checked', true);
    $('.sidebar_checkbox').attr('disabled', true);
    $('.always-sticky-header').attr('checked', true);
    $('.always-sticky-header').attr('disabled', true);
    $('.navbar_checkbox').attr('checked', true);
    $('.navbar_checkbox').attr('disabled', true);
  }

  $('.always-sticky-sidebar').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked == true) {
      $('.sidebar_checkbox').parent().addClass('checked disabled');
      $('.always-sticky-header').parent().addClass('checked disabled');
      $('.navbar_checkbox').parent().addClass('checked disabled');
      $('.sidebar_checkbox').attr('checked', true);
      $('.sidebar_checkbox').attr('disabled', true);
      $('.always-sticky-header').attr('checked', true);
      $('.always-sticky-header').attr('disabled', true);
      $('.navbar_checkbox').attr('checked', true);
      $('.navbar_checkbox').attr('disabled', true);

      if(typeof(Storage) !== "undefined") {
        /* Store */
        window.localStorage.setItem("always-sticky-sidebar", true);
        $('.header').addClass('fixed-navbar');
        $('.sidebar-v1').addClass('fixed-sidebar');
      }
    } else {
      window.localStorage.removeItem("always-sticky-sidebar");
      $('.header').removeClass('fixed-navbar');
      $('.sidebar-v1').removeClass('fixed-sidebar');
      $('.sidebar_checkbox').parent().removeClass('checked disabled');
      $('.always-sticky-header').parent().removeClass('checked disabled');
      $('.navbar_checkbox').parent().removeClass('checked disabled');
      $('.sidebar_checkbox').attr('checked', false);
      $('.sidebar_checkbox').attr('disabled', false);
      $('.always-sticky-header').attr('checked', false);
      $('.always-sticky-header').attr('disabled', false);
      $('.navbar_checkbox').attr('checked', false);
      $('.navbar_checkbox').attr('disabled', false);
    }
  });

  /***********************
   * Always Sticky Footer
   ***********************/
  if(window.localStorage.getItem("always-sticky-footer")) {
    $('.always-sticky-footer').prop('checked', true);
    $('.footer').addClass('fixed');
    $('.footer_checkbox').parent().addClass('checked disabled');
    $('.footer_checkbox').attr('checked', true);
    $('.footer_checkbox').attr('disabled', true);
  }

  $('.always-sticky-footer').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked == true) {
      $('.footer_checkbox').parent().addClass('checked disabled');
      $('.footer_checkbox').attr('checked', true);
      $('.footer_checkbox').attr('disabled', true);
      if(typeof(Storage) !== "undefined") {
        /* Store */
        window.localStorage.setItem("always-sticky-footer", true);
        $('.footer').addClass('fixed');
      }
    } else {
      window.localStorage.removeItem("always-sticky-footer");
      $('.footer').removeClass('fixed');
      $('.footer_checkbox').parent().removeClass('checked disabled');
      $('.footer_checkbox').attr('checked', false);
      $('.footer_checkbox').attr('disabled', false);
    }
  });

  /********************
   * Always Box-Layout
   ********************/
  if(window.localStorage.getItem("always-boxed-layout")) {
    $('.always-boxed-layout').prop('checked', true);
    $('body').addClass('boxed-layout');
    $('.box_checkbox').parent().addClass('checked disabled');
    $('.box_checkbox').attr('checked', true);
    $('.box_checkbox').attr('disabled', true);
  }
  $('.always-boxed-layout').iCheck({
    checkboxClass: 'icheckbox_theme',
    radioClass: 'iradio_square-blue'
  }).on('ifChanged', function(e) {
    var isChecked = e.currentTarget.checked;
    if(isChecked == true) {
      $('.box_checkbox').parent().addClass('checked disabled');
      $('.box_checkbox').attr('checked', true);
      $('.box_checkbox').attr('disabled', true);
      if(typeof(Storage) !== "undefined") {
        /* Store */
        window.localStorage.setItem("always-boxed-layout", true);
        $('body').addClass('boxed-layout');
      }
    } else {
      window.localStorage.removeItem("always-boxed-layout");
      $('body').removeClass('boxed-layout');
      $('.box_checkbox').parent().removeClass('checked disabled');
      $('.box_checkbox').attr('checked', false);
      $('.box_checkbox').attr('disabled', false);
    }
  });

  /**********************
   * Clear Local Storage
   **********************/
  $('.flush-local-storage').on("click", function(){
    $('.always-sticky-header').parent().removeClass('checked');
    $('.always-sticky-sidebar').parent().removeClass('checked');
    $('.always-sticky-footer').parent().removeClass('checked');
    $('.always-boxed-layout').parent().removeClass('checked');
    $('.navbar_checkbox').parent().removeClass('checked');
    $('.footer_checkbox').parent().removeClass('checked');
    $('.sidebar_checkbox').parent().removeClass('checked');
    $('.box_checkbox').parent().removeClass('checked');
    window.localStorage.clear();
    /* Remove all */
    $('.header').removeClass('fixed-navbar');
    $('.footer').removeClass('fixed');
    $('.sidebar-v1').removeClass('fixed-sidebar');
    $('body').removeClass('boxed-layout');

    $('.always-sticky-header').parent().removeClass('checked disabled');
    $('.always-sticky-header').attr('disabled', false);
    $('.navbar_checkbox').parent().removeClass('checked disabled');
    $('.navbar_checkbox').attr('disabled', false);
    $('.sidebar_checkbox').parent().removeClass('checked disabled');
    $('.sidebar_checkbox').attr('disabled', false);
    $('.footer_checkbox').parent().removeClass('checked disabled');
    $('.footer_checkbox').attr('disabled', false);
    $('.box_checkbox').parent().removeClass('checked disabled');
    $('.box_checkbox').attr('disabled', false);

    /* Toastr */
    Command: toastr["success"]("Flushed Local Storage");
  });
};