import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Dashboard
import { MainDashboardComponent } from "../components/dashboard/main-dashboard/main-dashboard.component";
import { DataAnalyticsDashboardComponent } from "../components/dashboard/data-analytics-dashboard/data-analytics-dashboard.component";
import { MarketingDashboardComponent } from "../components/dashboard/marketing-dashboard/marketing-dashboard.component";
import { SocialDashboardComponent } from "../components/dashboard/social-dashboard/social-dashboard.component";
import { ProjectDashboardComponent } from "../components/dashboard/project-dashboard/project-dashboard.component";
import { SalesDashboardComponent } from "../components/dashboard/sales-dashboard/sales-dashboard.component";

// UI Elements
import { ButtonsComponent } from "../components/ui-elements/buttons/buttons.component";
import { BootstrapCardsComponent } from "../components/ui-elements/bootstrap-cards/bootstrap-cards.component";
import { TabsComponent } from "../components/ui-elements/tabs/tabs.component";
import { TypographyComponent } from "../components/ui-elements/typography/typography.component";
import { ComponentsComponent } from "../components/ui-elements/components/components.component";
import { SocialButtonsComponent } from "../components/ui-elements/social-buttons/social-buttons.component";
import { CarouselComponent } from "../components/ui-elements/carousel/carousel.component";
import { ModalDialogsComponent } from "../components/ui-elements/modal-dialogs/modal-dialogs.component";
import { ScrollspyComponent } from "../components/ui-elements/scrollspy/scrollspy.component";

// Icons
import { FontawesomeIconsComponent } from "../components/icons/fontawesome-icons/fontawesome-icons.component";
import { SimpleLineIconsComponent } from "../components/icons/simple-line-icons/simple-line-icons.component";
import { IoniconsIconsComponent } from "../components/icons/ionicons-icons/ionicons-icons.component";
import { MaterialIconsComponent } from "../components/icons/material-icons/material-icons.component";

// Widgets
import { SocialWidgetsComponent } from "../components/widgets/social-widgets/social-widgets.component";
import { OtherWidgetsComponent } from "../components/widgets/other-widgets/other-widgets.component";
import { WeatherWidgetsComponent } from "../components/widgets/weather-widgets/weather-widgets.component";
import { MaterialCardsComponent } from "../components/widgets/material-cards/material-cards.component";

// Charts
import { AmChartsComponent } from "../components/charts/am-charts/am-charts.component";
import { ChartjsChartsComponent } from "../components/charts/chartjs-charts/chartjs-charts.component";
import { GoogleChartsComponent } from "../components/charts/google-charts/google-charts.component";
import { MorrisChartsComponent } from "../components/charts/morris-charts/morris-charts.component";
import { FlotChartsComponent } from "../components/charts/flot-charts/flot-charts.component";
import { RickshawChartsComponent } from "../components/charts/rickshaw-charts/rickshaw-charts.component";
import { EChartsComponent } from "../components/charts/e-charts/e-charts.component";
import { ChartistChartsComponent } from "../components/charts/chartist-charts/chartist-charts.component";
import { PeityAndSparklineChartComponent } from "../components/charts/peity-and-sparkline-chart/peity-and-sparkline-chart.component";

// Form And Plugins
import { FormElementsComponent } from "../components/forms-and-plugins/form-elements/form-elements.component";
import { FormAdvanceElementsComponent } from "../components/forms-and-plugins/form-advance-elements/form-advance-elements.component";
import { StepFormWizardComponent } from "../components/forms-and-plugins/step-form-wizard/step-form-wizard.component";
import { FileUploaderComponent } from "../components/forms-and-plugins/file-uploader/file-uploader.component";
import { SummernoteEditorsComponent } from "../components/forms-and-plugins/summernote-editors/summernote-editors.component";
import { CkEditorsComponent } from "../components/forms-and-plugins/ck-editors/ck-editors.component";
import { FormValidaionComponent } from "../components/forms-and-plugins/form-validaion/form-validaion.component";

// Plugins
import { ToastrAlertComponent } from "../components/plugins/toastr-alert/toastr-alert.component";
import { AnimateCssComponent } from "../components/plugins/animate-css/animate-css.component";
import { TinyconComponent } from "../components/plugins/tinycon/tinycon.component";
import { ClipboardComponent } from "../components/plugins/clipboard/clipboard.component";
import { IntroJsComponent } from "../components/plugins/intro-js/intro-js.component";
import { SlidersComponent } from "../components/plugins/sliders/sliders.component";
import { SortableComponentsComponent } from "../components/plugins/sortable-components/sortable-components.component";
import { SpinnerButtonsComponent } from "../components/plugins/spinner-buttons/spinner-buttons.component";
import { SweetModalsComponent } from "../components/plugins/sweet-modals/sweet-modals.component";
import { BarcodeGeneratorComponent } from "../components/plugins/barcode-generator/barcode-generator.component";
import { PageLoadersComponent } from "../components/plugins/page-loaders/page-loaders.component";
import { LiquidMeterComponent } from "../components/plugins/liquid-meter/liquid-meter.component";
import { ImageViewerComponent } from "../components/plugins/image-viewer/image-viewer.component";
import { SplitJsComponent } from "../components/plugins/split-js/split-js.component";
import { RatingComponent } from "../components/plugins/rating/rating.component";

// Maps
import { GoogleMapsComponent } from "../components/maps/google-maps/google-maps.component";
import { DataMapsComponent } from "../components/maps/data-maps/data-maps.component";

// Layouts
import { EmptyPageComponent } from "../components/layouts/empty-page/empty-page.component";
import { FullWidthLayoutComponent } from "../components/layouts/full-width-layout/full-width-layout.component";
import { FixedWidthLayoutComponent } from "../components/layouts/fixed-width-layout/fixed-width-layout.component";
import { HoizontalLayoutComponent } from "../components/layouts/hoizontal-layout/hoizontal-layout.component";
import { TabPageComponent } from "../components/layouts/tab-page/tab-page.component";
import { SidebarV2Component } from "../components/layouts/sidebar-v2/sidebar-v2.component";
import { NavbarV2Component } from "../components/layouts/navbar-v2/navbar-v2.component";

// pages
import { LoginComponent } from "../components/pages/login/login.component";
import { SignupComponent } from "../components/pages/signup/signup.component";
import { ForgotPasswordComponent } from "../components/pages/forgot-password/forgot-password.component";
import { ResetComponent } from "../components/pages/reset/reset.component";
import { LockScreenComponent } from "../components/pages/lock-screen/lock-screen.component";
import { Error404Component } from "../components/pages/error-404/error-404.component";
import { Error500Component } from "../components/pages/error-500/error-500.component";
import { BlogListComponent } from "../components/pages/blog-list/blog-list.component";
import { BlogComponent } from "../components/pages/blog/blog.component";
import { SupportsComponent } from "../components/pages/supports/supports.component";
import { ArticleComponent } from "../components/pages/article/article.component";
import { ForumComponent } from "../components/pages/forum/forum.component";
import { VoteListComponent } from "../components/pages/vote-list/vote-list.component";

// Tables
import { BootstrapTablesComponent } from "../components/tables/bootstrap-tables/bootstrap-tables.component";
import { FooTableComponent } from "../components/tables/foo-table/foo-table.component";
import { JsGridTableComponent } from "../components/tables/js-grid-table/js-grid-table.component";
import { DataTableComponent } from "../components/tables/data-table/data-table.component";

// Calendar
import { CalendarComponent } from "../components/calendar/calendar.component";

// MailBox
import { InboxComponent } from "../components/mailbox/inbox/inbox.component";
import { ReadmailComponent } from "../components/mailbox/readmail/readmail.component";

// E-Commerce
import { ProductListComponent } from "../components/e-commerce/product-list/product-list.component";
import { ProductEditComponent } from "../components/e-commerce/product-edit/product-edit.component";
import { ProductBoxComponent } from "../components/e-commerce/product-box/product-box.component";
import { ProductDetailComponent } from "../components/e-commerce/product-detail/product-detail.component";
import { CartComponent } from "../components/e-commerce/cart/cart.component";
import { OrderListComponent } from "../components/e-commerce/order-list/order-list.component";
import { OrderDetailComponent } from "../components/e-commerce/order-detail/order-detail.component";
import { PaymentsComponent } from "../components/e-commerce/payments/payments.component";
import { InvoiceComponent } from "../components/e-commerce/invoice/invoice.component";

// Project Management
import { ProjectListComponent } from "../components/projet-management/project-list/project-list.component";
import { ProjectDetailComponent } from "../components/projet-management/project-detail/project-detail.component";
import { ClientsComponent } from "../components/projet-management/clients/clients.component";
import { IssueListComponent } from "../components/projet-management/issue-list/issue-list.component";
import { IssueDetailComponent } from "../components/projet-management/issue-detail/issue-detail.component";
import { ProjectBoardComponent } from "../components/projet-management/project-board/project-board.component";
import { AgileBoardComponent } from "../components/projet-management/agile-board/agile-board.component";

// User Management
import { UserListComponent } from "../components/user-management/user-list/user-list.component";
import { UserEditComponent } from "../components/user-management/user-edit/user-edit.component";

// Miscellaneous Page
import { ContactCardsComponent } from "../components/miscellaneous-page/contact-cards/contact-cards.component";
import { FaqComponent } from "../components/miscellaneous-page/faq/faq.component";
import { ChatViewComponent } from "../components/miscellaneous-page/chat-view/chat-view.component";
import { HorizontalTimelineComponent } from "../components/miscellaneous-page/horizontal-timeline/horizontal-timeline.component";
import { VerticalTimelineComponent } from "../components/miscellaneous-page/vertical-timeline/vertical-timeline.component";
import { PdfViewerComponent } from "../components/miscellaneous-page/pdf-viewer/pdf-viewer.component";
import { PricingComponent } from "../components/miscellaneous-page/pricing/pricing.component";
import { SearchPageComponent } from "../components/miscellaneous-page/search-page/search-page.component";
import { VideoPageComponent } from "../components/miscellaneous-page/video-page/video-page.component";

// Image Gallery
import { ImageGalleryComponent } from "../components/image-gallery/image-gallery.component";

// File Manager
import { FileManagerComponent } from "../components/file-manager/file-manager.component";

// Profile
import { ProfileComponent } from "../components/profile/profile.component";

// Documents
import { BootstrapGridComponent } from "../components/documents/bootstrap-grid/bootstrap-grid.component";
import { HelperClassesComponent } from "../components/documents/helper-classes/helper-classes.component";

import { AuthGuard, NonAuthGuard } from "../guard/guard"

export const routes: Routes = [
	{
		path: '',
		children: [
			// Dashboard
			{ path: '', canActivate: [NonAuthGuard], component: LoginComponent, data: { title: 'Login | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'dashboard', canActivate: [AuthGuard], component: MainDashboardComponent, data: { title: 'Main Dashboard | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'dashboard/main-dashboard', canActivate: [AuthGuard], component: MainDashboardComponent, data: { title: 'Main Dashboard | Ranpariya Multipurpose Responsive Theme' } },
            { path: 'dashboard/data-analytics-dashboard', canActivate: [AuthGuard], component: DataAnalyticsDashboardComponent, data: { title: 'Data Analytics Dashboard | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'dashboard/marketing-dashboard', canActivate: [AuthGuard], component: MarketingDashboardComponent, data: { title: 'Marketing Dashboard | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'dashboard/social-dashboard', canActivate: [AuthGuard], component: SocialDashboardComponent, data: { title: 'Social Dashboard | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'dashboard/project-dashboard', canActivate: [AuthGuard], component: ProjectDashboardComponent, data: { title: 'Project Dashboard | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'dashboard/sales-dashboard', canActivate: [AuthGuard], component: SalesDashboardComponent, data: { title: 'Sales Dashboard | Ranpariya Multipurpose Responsive Theme' } },

			// UI Elements
			{ path: 'ui-elements', canActivate: [AuthGuard], component: ButtonsComponent, data: { title: 'Buttons | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/buttons', canActivate: [AuthGuard], component: ButtonsComponent, data: { title: 'Buttons | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/bootstrap-cards', canActivate: [AuthGuard], component: BootstrapCardsComponent, data: { title: 'Bootstrap Cards | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/tabs', canActivate: [AuthGuard], component: TabsComponent, data: { title: 'UI Tabs | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/typography', canActivate: [AuthGuard], component: TypographyComponent, data: { title: 'Typography | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/components', component: ComponentsComponent, data: { title: 'Components | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/social-buttons', canActivate: [AuthGuard], component: SocialButtonsComponent, data: { title: 'Social Buttons | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/carousel', canActivate: [AuthGuard], component: CarouselComponent, data: { title: 'Carousel | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/modal-dialogs', canActivate: [AuthGuard], component: ModalDialogsComponent, data: { title: 'Modals | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'ui-elements/scrollspy', canActivate: [AuthGuard], component: ScrollspyComponent, data: { title: 'Scrollspy Page | Ranpariya Multipurpose Responsive Theme' } },

			// Icons
			{ path: 'icons/fontawesome-icons', canActivate: [AuthGuard], component: FontawesomeIconsComponent, data: { title: 'Font Awesome Icons | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'icons/simple-line-icons', canActivate: [AuthGuard], component: SimpleLineIconsComponent, data: { title: 'Simple Line Icons | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'icons/ionicons-icons', canActivate: [AuthGuard], component: IoniconsIconsComponent, data: { title: 'Ionicons Icons | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'icons/material-icons', canActivate: [AuthGuard], component: MaterialIconsComponent, data: { title: 'Material Icons | Ranpariya Multipurpose Responsive Theme' } },

			// Widgets
			{ path: 'widgets', canActivate: [AuthGuard], component: SocialWidgetsComponent, data: { title: 'Social Widgets | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'widgets/social-widgets', canActivate: [AuthGuard], component: SocialWidgetsComponent, data: { title: 'Social Widgets | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'widgets/other-widgets', canActivate: [AuthGuard], component: OtherWidgetsComponent, data: { title: 'Other Widgets | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'widgets/weather-widgets', canActivate: [AuthGuard], component: WeatherWidgetsComponent, data: { title: 'Weather Widgets | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'widgets/material-cards', canActivate: [AuthGuard], component: MaterialCardsComponent, data: { title: 'Material Cards | Ranpariya Multipurpose Responsive Theme' } },

			// Charts
			{ path: 'charts', canActivate: [AuthGuard], component: AmChartsComponent, data: { title: 'AM Charts | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/am-charts', canActivate: [AuthGuard], component: AmChartsComponent, data: { title: 'AM Charts | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/chartjs-charts', canActivate: [AuthGuard], component: ChartjsChartsComponent, data: { title: 'Chartsjs | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/google-charts', canActivate: [AuthGuard], component: GoogleChartsComponent, data: { title: 'Google Charts | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/morris-charts', canActivate: [AuthGuard], component: MorrisChartsComponent, data: { title: 'Morris Charts | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/flot-charts', canActivate: [AuthGuard], component: FlotChartsComponent, data: { title: 'Flot Charts | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/rickshaw-charts', canActivate: [AuthGuard], component: RickshawChartsComponent, data: { title: 'Rickshaw Charts | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/e-charts', canActivate: [AuthGuard], component: EChartsComponent, data: { title: 'E-Charts | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/chartist-charts', canActivate: [AuthGuard], component: ChartistChartsComponent, data: { title: 'Chartist Charts | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'charts/peity-and-sparkline-chart', canActivate: [AuthGuard], component: PeityAndSparklineChartComponent, data: { title: 'Peity and Sparkline Charts | Ranpariya Multipurpose Responsive Theme' } },

			// Forms And Plugins
			{ path: 'forms-and-plugins', canActivate: [AuthGuard], component: FormElementsComponent, data: { title: 'Form Elements | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'forms-and-plugins/form-elements', canActivate: [AuthGuard], component: FormElementsComponent, data: { title: 'Form Elements | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'forms-and-plugins/form-advance-elements', canActivate: [AuthGuard], component: FormAdvanceElementsComponent, data: { title: 'Form Advance Elements | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'forms-and-plugins/step-form-wizard', canActivate: [AuthGuard], component: StepFormWizardComponent, data: { title: 'Form Wizard | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'forms-and-plugins/file-uploader', canActivate: [AuthGuard], component: FileUploaderComponent, data: { title: 'File Uploader | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'forms-and-plugins/summernote-editors', canActivate: [AuthGuard], component: SummernoteEditorsComponent, data: { title: 'Summernote Editors | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'forms-and-plugins/ck-editors', canActivate: [AuthGuard], component: CkEditorsComponent, data: { title: 'CK Editor | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'forms-and-plugins/form-validaion', canActivate: [AuthGuard], component: FormValidaionComponent, data: { title: 'Validation Form | Ranpariya Multipurpose Responsive Theme' } },

			// Plugins
			{ path: 'plugins', canActivate: [AuthGuard], component: ToastrAlertComponent, data: { title: 'Toastr | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/toastr-alert', canActivate: [AuthGuard], component: ToastrAlertComponent, data: { title: 'Toastr | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/animate-css', canActivate: [AuthGuard], component: AnimateCssComponent, data: { title: 'Animate-CSS | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/tinycon', canActivate: [AuthGuard], component: TinyconComponent, data: { title: 'Tinycon And Blink Title | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/clipboard', canActivate: [AuthGuard], component: ClipboardComponent, data: { title: 'Clipboard | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/intro-js', canActivate: [AuthGuard], component: IntroJsComponent, data: { title: 'Intro Js | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/sliders', canActivate: [AuthGuard], component: SlidersComponent, data: { title: 'Sliders | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/sortable-components', canActivate: [AuthGuard], component: SortableComponentsComponent, data: { title: 'Sortable Components | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/spinner-buttons', canActivate: [AuthGuard], component: SpinnerButtonsComponent, data: { title: 'Spinner Buttons | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/sweet-modals', canActivate: [AuthGuard], component: SweetModalsComponent, data: { title: 'Sweet Alert | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/barcode-generator', canActivate: [AuthGuard], component: BarcodeGeneratorComponent, data: { title: 'Barcode Generator | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/page-loaders', canActivate: [AuthGuard], component: PageLoadersComponent, data: { title: 'Loading Bars | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/liquid-meter', canActivate: [AuthGuard], component: LiquidMeterComponent, data: { title: 'Liquid Meter | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/image-viewer', canActivate: [AuthGuard], component: ImageViewerComponent, data: { title: 'Image Viewer | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/split-js', canActivate: [AuthGuard], component: SplitJsComponent, data: { title: 'Split JS | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'plugins/rating', canActivate: [AuthGuard], component: RatingComponent, data: { title: 'Rating | Ranpariya Multipurpose Responsive Theme' } },

			// Maps
			{ path: 'maps', canActivate: [AuthGuard], component: GoogleMapsComponent, data: { title: 'Google Maps | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'maps/google-maps', canActivate: [AuthGuard], component: GoogleMapsComponent, data: { title: 'Google Maps | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'maps/data-maps', canActivate: [AuthGuard], component: DataMapsComponent, data: { title: 'Data Maps | Ranpariya Multipurpose Responsive Theme' } },

			// Layouts
			{ path: 'layouts', canActivate: [AuthGuard], component: EmptyPageComponent, data: { title: 'Empty Page | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'layouts/empty-page', canActivate: [AuthGuard], component: EmptyPageComponent, data: { title: 'Empty Page | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'layouts/full-width-layout', canActivate: [AuthGuard], component: FullWidthLayoutComponent, data: { title: 'Full Width Layout | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'layouts/fixed-width-layout', canActivate: [AuthGuard], component: FixedWidthLayoutComponent, data: { title: 'Fixed Width Layout | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'layouts/hoizontal-layout', canActivate: [AuthGuard], component: HoizontalLayoutComponent, data: { title: 'Horizontal Layout | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'layouts/tab-page', canActivate: [AuthGuard], component: TabPageComponent, data: { title: 'Tab Page | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'layouts/sidebar-v2', canActivate: [AuthGuard], component: SidebarV2Component, data: { title: 'Sidebar v2 | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'layouts/navbar-v2', canActivate: [AuthGuard], component: NavbarV2Component, data: { title: 'Navbar v2 | Ranpariya Multipurpose Responsive Theme' } },

			// Pages
			{ path: 'pages', canActivate: [AuthGuard], component: BlogListComponent, data: { title: 'Blog List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/login', canActivate: [NonAuthGuard], component: LoginComponent, data: { title: 'Login page | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/signup', canActivate: [NonAuthGuard], component: SignupComponent, data: { title: 'Register page | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/forgot-password', canActivate: [NonAuthGuard], component: ForgotPasswordComponent, data: { title: 'Forgot Password | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'reset/:token', canActivate: [NonAuthGuard], component: ResetComponent, data: { title: 'Reset Password | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/lock-screen', canActivate: [AuthGuard], component: LockScreenComponent, data: { title: 'Lock Screen | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/error-404', canActivate: [NonAuthGuard], component: Error404Component, data: { title: '404 | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/error-500', canActivate: [NonAuthGuard], component: Error500Component, data: { title: '500 | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/blog-list', canActivate: [AuthGuard], component: BlogListComponent, data: { title: 'Blog List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/blog', canActivate: [AuthGuard], component: BlogComponent, data: { title: 'Blog | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/supports', canActivate: [AuthGuard], component: SupportsComponent, data: { title: 'Supports | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/article', canActivate: [AuthGuard], component: ArticleComponent, data: { title: 'Article | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/forum', canActivate: [AuthGuard], component: ForumComponent, data: { title: 'Forum | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'pages/vote-list', canActivate: [AuthGuard], component: VoteListComponent, data: { title: 'Vote List | Ranpariya Multipurpose Responsive Theme' } },

			// Tables
			{ path: 'tables', canActivate: [AuthGuard], component: BootstrapTablesComponent, data: { title: 'Tables | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'tables/bootstrap-tables', canActivate: [AuthGuard], component: BootstrapTablesComponent, data: { title: 'Tables | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'tables/foo-table', canActivate: [AuthGuard], component: FooTableComponent, data: { title: 'Foo Table | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'tables/js-grid-table', canActivate: [AuthGuard], component: JsGridTableComponent, data: { title: 'JS Grid Table | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'tables/data-table', canActivate: [AuthGuard], component: DataTableComponent, data: { title: 'Data Table | Ranpariya Multipurpose Responsive Theme' } },

			// Calendar
			{ path: 'calendar', canActivate: [AuthGuard], component: CalendarComponent, data: { title: 'Calendar | Ranpariya Multipurpose Responsive Theme' } },

			// MailBox
			{ path: 'mailbox', canActivate: [AuthGuard], component: InboxComponent, data: { title: 'Mail Inbox | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'mailbox/inbox', canActivate: [AuthGuard], component: InboxComponent, data: { title: 'Mail Inbox | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'mailbox/readmail', canActivate: [AuthGuard], component: ReadmailComponent, data: { title: 'Read Mail | Ranpariya Multipurpose Responsive Theme' } },

			// E-Commerce
			{ path: 'e-commerce', canActivate: [AuthGuard], component: ProductListComponent, data: { title: 'Product List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/product-list', canActivate: [AuthGuard], component: ProductListComponent, data: { title: 'Product List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/product-edit', canActivate: [AuthGuard], component: ProductEditComponent, data: { title: 'Product Edit | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/product-box', canActivate: [AuthGuard], component: ProductBoxComponent, data: { title: 'Product Grid | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/product-detail', canActivate: [AuthGuard], component: ProductDetailComponent, data: { title: 'Product Detail | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/cart', canActivate: [AuthGuard], component: CartComponent, data: { title: 'Cart | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/order-list', canActivate: [AuthGuard], component: OrderListComponent, data: { title: 'Order List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/order-detail', canActivate: [AuthGuard], component: OrderDetailComponent, data: { title: 'Order Detail | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/payments', canActivate: [AuthGuard], component: PaymentsComponent, data: { title: 'Payments | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'e-commerce/invoice', canActivate: [AuthGuard], component: InvoiceComponent, data: { title: 'Invoice | Ranpariya Multipurpose Responsive Theme' } },

			// Project Managements
			{ path: 'projet-management', canActivate: [AuthGuard], component: ProjectListComponent, data: { title: 'Project List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'projet-management/project-list', canActivate: [AuthGuard], component: ProjectListComponent, data: { title: 'Project List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'projet-management/project-detail', canActivate: [AuthGuard], component: ProjectDetailComponent, data: { title: 'Project Detail | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'projet-management/clients', canActivate: [AuthGuard], component: ClientsComponent, data: { title: 'Clients | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'projet-management/issue-list', canActivate: [AuthGuard], component: IssueListComponent, data: { title: 'Issue List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'projet-management/issue-detail', canActivate: [AuthGuard], component: IssueDetailComponent, data: { title: 'Issue Detail | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'projet-management/project-board', canActivate: [AuthGuard], component: ProjectBoardComponent, data: { title: 'Project Board | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'projet-management/agile-board', canActivate: [AuthGuard], component: AgileBoardComponent, data: { title: 'Agile Board | Ranpariya Multipurpose Responsive Theme' } },

			// User Managements
			{ path: 'user-management', canActivate: [AuthGuard], component: UserListComponent, data: { title: 'Users List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'user-management/user-list', canActivate: [AuthGuard], component: UserListComponent, data: { title: 'Users List | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'user-management/user-edit', canActivate: [AuthGuard], component: UserEditComponent, data: { title: 'User Edit | Ranpariya Multipurpose Responsive Theme' } },

			// Miscellaneous Page
			{ path: 'miscellaneous-page', canActivate: [AuthGuard], component: ContactCardsComponent, data: { title: 'Contact Cards | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/contact-cards', canActivate: [AuthGuard], component: ContactCardsComponent, data: { title: 'Contact Cards | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/faq', canActivate: [AuthGuard], component: FaqComponent, data: { title: 'FAQ | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/chat-view', canActivate: [AuthGuard], component: ChatViewComponent, data: { title: 'Chat View | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/horizontal-timeline', canActivate: [AuthGuard], component: HorizontalTimelineComponent, data: { title: 'Horizontal Timeline | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/vertical-timeline', canActivate: [AuthGuard], component: VerticalTimelineComponent, data: { title: 'Vertical Timeline | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/pdf-viewer', canActivate: [AuthGuard], component: PdfViewerComponent, data: { title: 'PDF Viewer | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/pricing', canActivate: [AuthGuard], component: PricingComponent, data: { title: 'Pricing | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/search-page', canActivate: [AuthGuard], component: SearchPageComponent, data: { title: 'Search Page | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'miscellaneous-page/video-page', canActivate: [AuthGuard], component: VideoPageComponent, data: { title: 'Video Page | Ranpariya Multipurpose Responsive Theme' } },

			// Image Gallery
			{ path: 'image-gallery', canActivate: [AuthGuard], component: ImageGalleryComponent, data: { title: 'Image Gallery | Ranpariya Multipurpose Responsive Theme' } },

			// File Manager
			{ path: 'file-manager', canActivate: [AuthGuard], component: FileManagerComponent, data: { title: 'File Manager | Ranpariya Multipurpose Responsive Theme' } },

			// Profile
			{ path: 'profile', canActivate: [AuthGuard], component: ProfileComponent, data: { title: 'Profile | Ranpariya Multipurpose Responsive Theme' } },

			// Documents
			{ path: 'documents', canActivate: [AuthGuard], component: BootstrapGridComponent, data: { title: 'Grid Layout | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'documents/bootstrap-grid', canActivate: [AuthGuard], component: BootstrapGridComponent, data: { title: 'Grid Layout | Ranpariya Multipurpose Responsive Theme' } },
			{ path: 'documents/helper-classes', canActivate: [AuthGuard], component: HelperClassesComponent, data: { title: 'Helper Classes | Ranpariya Multipurpose Responsive Theme' } },
		]
	}
]

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true })],
	exports: [RouterModule]
})
export class RouteRoutingModule { }
