import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JsGridTableComponent } from './js-grid-table.component';

describe('JsGridTableComponent', () => {
  let component: JsGridTableComponent;
  let fixture: ComponentFixture<JsGridTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsGridTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsGridTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
