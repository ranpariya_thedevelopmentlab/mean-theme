import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-js-grid-table',
  templateUrl: './js-grid-table.component.html',
  styleUrls: ['./js-grid-table.component.css']
})
export class JsGridTableComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Tables";
    globals.childMenu ="JSGridTable";
  }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/JS-Grid-Data.min.js').then(function (JSGridData) {
      JSGridData();
    });
    SystemJS.import('/assets/dist/js/JS-Grid.min.js').then(function (JSGrid) {
      JSGrid();
    });
  }
}