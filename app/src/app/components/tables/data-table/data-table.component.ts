import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Tables";
    globals.childMenu ="DataTables";
  }

  ngOnInit() {
    $('#data-table').DataTable({
      "lengthMenu": [[10, 15, 20, 25, -1], [10, 15, 20, 25, "All"]]
    });
  }
}