import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-bootstrap-tables',
  templateUrl: './bootstrap-tables.component.html',
  styleUrls: ['./bootstrap-tables.component.css']
})
export class BootstrapTablesComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Tables";
    globals.childMenu ="BootstrapTables";
  }

  ngOnInit() {
  }
}