import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-foo-table',
  templateUrl: './foo-table.component.html',
  styleUrls: ['./foo-table.component.css']
})
export class FooTableComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Tables";
    globals.childMenu ="FooTable";
  }

  ngOnInit() {
    $('.foo-table').footable();
  }
}