import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooTableComponent } from './foo-table.component';

describe('FooTableComponent', () => {
  let component: FooTableComponent;
  let fixture: ComponentFixture<FooTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
