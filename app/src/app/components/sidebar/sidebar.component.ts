import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../globals';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private globals: Globals) { }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/sidebar.min.js').then(function (sidebarV1) {
      sidebarV1();
    });
  }
}