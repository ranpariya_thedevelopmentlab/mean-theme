import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.css']
})
export class MainDashboardComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Dashboards";
    globals.childMenu ="MainDashboard";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/social-widgets.min.js').then(function (socialWidgets) {
      socialWidgets();
    });

    SystemJS.import('/assets/dist/js/main-dashboard.min.js').then(function (mainDashboard) {
      mainDashboard();
    });
  }
}