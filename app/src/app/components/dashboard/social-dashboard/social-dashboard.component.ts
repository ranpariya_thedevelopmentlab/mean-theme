import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-social-dashboard',
  templateUrl: './social-dashboard.component.html',
  styleUrls: ['./social-dashboard.component.css']
})
export class SocialDashboardComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Dashboards";
    globals.childMenu ="SocialDashboard";
  }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/social-widgets.min.js').then(function (socialWidgets) {
      socialWidgets();
    });

    SystemJS.import('/assets/dist/js/social-dashboard.min.js').then(function (socialDashboard) {
      socialDashboard();
    });
  }
}