import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataAnalyticsDashboardComponent } from './data-analytics-dashboard.component';

describe('DataAnalyticsDashboardComponent', () => {
  let component: DataAnalyticsDashboardComponent;
  let fixture: ComponentFixture<DataAnalyticsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataAnalyticsDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataAnalyticsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
