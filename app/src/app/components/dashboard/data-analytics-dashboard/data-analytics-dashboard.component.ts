import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-data-analytics-dashboard',
  templateUrl: './data-analytics-dashboard.component.html',
  styleUrls: ['./data-analytics-dashboard.component.css']
})
export class DataAnalyticsDashboardComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Dashboards";
    globals.childMenu ="DataAnalyticsDashboard";
  }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/social-widgets.min.js').then(function (socialWidgets) {
      socialWidgets();
    });

    SystemJS.import('/assets/dist/js/data-analytics-dashboard.min.js').then(function (dataAnalyticsDashboard) {
      dataAnalyticsDashboard();
    });
  }
}