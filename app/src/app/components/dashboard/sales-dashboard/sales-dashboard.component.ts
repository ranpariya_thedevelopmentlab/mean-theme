import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-sales-dashboard',
  templateUrl: './sales-dashboard.component.html',
  styleUrls: ['./sales-dashboard.component.css']
})
export class SalesDashboardComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Dashboards";
    globals.childMenu ="SalesDashboard";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/social-widgets.min.js').then(function (socialWidgets) {
      socialWidgets();
    });

    SystemJS.import('/assets/dist/js/sales-dashboard.min.js').then(function (salesDashboard) {
      salesDashboard();
    });

    SystemJS.import('/assets/dist/js/horizontal-timeline.min.js').then(function (horizontalTimeline) {
      horizontalTimeline();
    });
  }
}