import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-marketing-dashboard',
  templateUrl: './marketing-dashboard.component.html',
  styleUrls: ['./marketing-dashboard.component.css']
})
export class MarketingDashboardComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Dashboards";
    globals.childMenu ="MarketingDashboard";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/social-widgets.min.js').then(function (socialWidgets) {
      socialWidgets();
    });

    SystemJS.import('/assets/dist/js/marketing-dashboard.min.js').then(function (marketingDashboard) {
      marketingDashboard();
    });
  }
}