import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-social-widgets',
  templateUrl: './social-widgets.component.html',
  styleUrls: ['./social-widgets.component.css']
})
export class SocialWidgetsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Widgets";
    globals.childMenu ="SocialWidgets";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/social-widgets.min.js').then(function (socialWidgets) {
      socialWidgets();
    });
    SystemJS.import('/assets/dist/js/components.min.js').then(function (components) {
      components();
    });
  }
}