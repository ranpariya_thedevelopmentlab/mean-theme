import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-weather-widgets',
  templateUrl: './weather-widgets.component.html',
  styleUrls: ['./weather-widgets.component.css']
})
export class WeatherWidgetsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Widgets";
    globals.childMenu ="WeatherWidgets";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/weather-widgets.min.js').then(function (weatherWidgets) {
      weatherWidgets();
    });
  }
}