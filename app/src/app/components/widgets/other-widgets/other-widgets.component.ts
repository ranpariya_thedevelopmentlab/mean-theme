import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-other-widgets',
  templateUrl: './other-widgets.component.html',
  styleUrls: ['./other-widgets.component.css']
})
export class OtherWidgetsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Widgets";
    globals.childMenu ="OtherWidgets";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/social-widgets.min.js').then(function (socialWidgets) {
      socialWidgets();
    });
    SystemJS.import('/assets/dist/js/other-widgets.min.js').then(function (otherWidgets) {
      otherWidgets();
    });
  }
}