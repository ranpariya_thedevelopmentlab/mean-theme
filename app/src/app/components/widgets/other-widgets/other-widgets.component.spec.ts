import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherWidgetsComponent } from './other-widgets.component';

describe('OtherWidgetsComponent', () => {
  let component: OtherWidgetsComponent;
  let fixture: ComponentFixture<OtherWidgetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherWidgetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherWidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
