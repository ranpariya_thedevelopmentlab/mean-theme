import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-material-cards',
  templateUrl: './material-cards.component.html',
  styleUrls: ['./material-cards.component.css']
})
export class MaterialCardsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Widgets";
    globals.childMenu ="MaterialCards";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/material-cards.min.js').then(function (materialCards) {
      materialCards();
    });
  }
}