import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../globals';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Calendar";
    globals.childMenu ="Calendar";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/calendar.min.js').then(function (calendar) {
      calendar();
    });
  }
}