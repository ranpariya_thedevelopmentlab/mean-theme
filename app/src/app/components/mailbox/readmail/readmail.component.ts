import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-readmail',
  templateUrl: './readmail.component.html',
  styleUrls: ['./readmail.component.css']
})
export class ReadmailComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="MailBox";
    globals.childMenu ="ReadMail";
  }

  ngOnInit() {
  }
}