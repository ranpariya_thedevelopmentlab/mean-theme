import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="MailBox";
    globals.childMenu ="Inbox";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/mail.min.js').then(function (mailBox) {
      mailBox();
    });
    SystemJS.import('/assets/dist/js/components.min.js').then(function (components) {
      components();
    });
  }
}