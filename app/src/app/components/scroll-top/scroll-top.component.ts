import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';

@Component({
  selector: 'app-scroll-top',
  templateUrl: './scroll-top.component.html',
  styleUrls: ['./scroll-top.component.css']
})
export class ScrollTopComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/scroll-top.min.js').then(function (scrollTop) {
      scrollTop();
    });
  }
}