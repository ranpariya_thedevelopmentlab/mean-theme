import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user: any;
  constructor(private _router: Router, private http: HttpClient, private _cookieService:CookieService) {
    if(this._cookieService.get('user')){
      if(this._cookieService.get('user').substr(0, 2) == 'j:') {
        this.user = JSON.parse(this._cookieService.get('user').replace('j:', ''));
      } else {
        this.user = JSON.parse(this._cookieService.get('user'));
      }
    }
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/animate.min.js').then(function (animateJS) {
      animateJS();
    });
    SystemJS.import('/assets/dist/js/custom-scrollbar.min.js').then(function (customScroll) {
      customScroll();
    });
  }

  // --
  // Lock Screen
  lock(){
    this._router.navigateByUrl('/pages/lock-screen');
  }

  // --
  // Logout user
  logout() {
    this.http.post('/logout', {}).subscribe(data => {
      this._cookieService.removeAll();
      this._router.navigateByUrl('/');
    });
  };
}