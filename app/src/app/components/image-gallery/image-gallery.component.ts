import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.css']
})
export class ImageGalleryComponent implements OnInit {

  imageGallery: string;
  constructor(private globals: Globals) {
    globals.parentMenu ="ImageGallery";
    globals.childMenu ="ImageGallery";
    this.imageGallery = `<div class="img-gallery ih-item square effect1 left_and_right">
  <a href="#">
    <div class="img">
      <img src="..." alt="img">
    </div>
    <div class="info">
      <h3>Heading here</h3>
      <p>Description goes here</p>
    </div>
  </a>
</div>`
  }

  ngOnInit() {
  }
}