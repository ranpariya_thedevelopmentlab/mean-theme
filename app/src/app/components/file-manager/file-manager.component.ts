import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../globals';

@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.css']
})
export class FileManagerComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="FileManager";
    globals.childMenu ="FileManager";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/file-manager.min.js').then(function (fileManager) {
      fileManager();
    });
  }
}