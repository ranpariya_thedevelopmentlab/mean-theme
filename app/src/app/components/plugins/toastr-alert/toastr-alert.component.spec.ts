import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToastrAlertComponent } from './toastr-alert.component';

describe('ToastrAlertComponent', () => {
  let component: ToastrAlertComponent;
  let fixture: ComponentFixture<ToastrAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToastrAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToastrAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
