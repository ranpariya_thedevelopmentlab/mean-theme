import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-toastr-alert',
  templateUrl: './toastr-alert.component.html',
  styleUrls: ['./toastr-alert.component.css']
})
export class ToastrAlertComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="ToastrAlert";
  }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/toastr.min.js').then(function (toastr_alert) {
      toastr_alert();
    });
  }
}