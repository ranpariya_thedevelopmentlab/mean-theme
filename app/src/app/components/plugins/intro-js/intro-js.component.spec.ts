import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroJsComponent } from './intro-js.component';

describe('IntroJsComponent', () => {
  let component: IntroJsComponent;
  let fixture: ComponentFixture<IntroJsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroJsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroJsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
