import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-intro-js',
  templateUrl: './intro-js.component.html',
  styleUrls: ['./intro-js.component.css']
})
export class IntroJsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="IntroJS";
  }

  ngOnInit() {
  }
}