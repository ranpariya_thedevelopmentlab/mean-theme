import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitJsComponent } from './split-js.component';

describe('SplitJsComponent', () => {
  let component: SplitJsComponent;
  let fixture: ComponentFixture<SplitJsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitJsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitJsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
