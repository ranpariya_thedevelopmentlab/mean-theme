import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-split-js',
  templateUrl: './split-js.component.html',
  styleUrls: ['./split-js.component.css']
})
export class SplitJsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="SplitJS";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/splitJS.min.js').then(function (splitJS) {
      splitJS();
    });
  }
}