import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-spinner-buttons',
  templateUrl: './spinner-buttons.component.html',
  styleUrls: ['./spinner-buttons.component.css']
})
export class SpinnerButtonsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="SpinnerButtons";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/spinner-buttons.min.js').then(function (spinnerButtons) {
      spinnerButtons();
    });
  }
}