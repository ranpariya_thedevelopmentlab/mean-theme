import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-tinycon',
  templateUrl: './tinycon.component.html',
  styleUrls: ['./tinycon.component.css']
})
export class TinyconComponent implements OnInit {
  tinycon : string;
  tinyconAmdSupport : string;
  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="Tinycon";

    this.tinycon = `Tinycon.setOptions({
    width: 7,
    height: 9,
    font: '10px arial',
    color: '#ffffff',
    background: '#FF0000',
    fallback: true
});`;

    this.tinyconAmdSupport = `require([
    'tinycon.js'
], function (T) {

    T.setOptions({
        width: 7,
        height: 9,
        font: '10px arial',
        color: '#ffffff',
        background: '#549A2F',
        fallback: true
    });

    T.setBubble(7);

});`;
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/tinycon.min.js').then(function (tinycon) {
      tinycon();
    });
  }
}