import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TinyconComponent } from './tinycon.component';

describe('TinyconComponent', () => {
  let component: TinyconComponent;
  let fixture: ComponentFixture<TinyconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinyconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinyconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
