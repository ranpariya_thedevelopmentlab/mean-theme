import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-clipboard',
  templateUrl: './clipboard.component.html',
  styleUrls: ['./clipboard.component.css']
})
export class ClipboardComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="Clipboard";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/clipboard.min.js').then(function (clipboard) {
      clipboard();
    });
  }
}