import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-liquid-meter',
  templateUrl: './liquid-meter.component.html',
  styleUrls: ['./liquid-meter.component.css']
})
export class LiquidMeterComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="LiquidMeter";
  }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/liquid-meter.min.js').then(function (liquidMeter) {
      liquidMeter();
    });
  }
}