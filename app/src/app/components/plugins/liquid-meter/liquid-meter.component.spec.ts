import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidMeterComponent } from './liquid-meter.component';

describe('LiquidMeterComponent', () => {
  let component: LiquidMeterComponent;
  let fixture: ComponentFixture<LiquidMeterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidMeterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidMeterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
