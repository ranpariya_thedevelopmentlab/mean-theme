import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-sortable-components',
  templateUrl: './sortable-components.component.html',
  styleUrls: ['./sortable-components.component.css']
})
export class SortableComponentsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="SortableComponents";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/sortble-list.min.js').then(function (sortbleList) {
      sortbleList();
    });
  }
}