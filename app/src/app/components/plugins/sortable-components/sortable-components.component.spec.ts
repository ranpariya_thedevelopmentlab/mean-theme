import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortableComponentsComponent } from './sortable-components.component';

describe('SortableComponentsComponent', () => {
  let component: SortableComponentsComponent;
  let fixture: ComponentFixture<SortableComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortableComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortableComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
