import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SweetModalsComponent } from './sweet-modals.component';

describe('SweetModalsComponent', () => {
  let component: SweetModalsComponent;
  let fixture: ComponentFixture<SweetModalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SweetModalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SweetModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
