import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';
import 'prismjs/prism';

@Component({
  selector: 'app-sweet-modals',
  templateUrl: './sweet-modals.component.html',
  styleUrls: ['./sweet-modals.component.css']
})
export class SweetModalsComponent implements OnInit {

  alertSuccess : string;
  alertWarning : string;
  prompt : string;
  alertError : string;
  alertConfirm : string;
  alert : string;
  alertConfirmTitle : string;
  promptValue : string;
  alertVideo : string;
  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="SweetModals";

    this.alertSuccess = `$.sweetModal({
      content: 'Success icon for Alert Success.',
      icon: $.sweetModal.ICON_SUCCESS
    });`;
    this.alertWarning = `$.sweetModal({
      content: 'Warning icon for Alert Warning.',
      icon: $.sweetModal.ICON_WARNING
    });`;
    this.prompt = `$.sweetModal.prompt('Can I help you?', null, null, function(val) {
      $.sweetModal('You typed: ' + val);
    });`;
    this.alertError = `$.sweetModal({
      content: 'This is an Error Icon.',
      title: 'Error Detail',
      icon: $.sweetModal.ICON_ERROR,

      buttons: [
        {
          label: 'OK',
          classes: 'redB'
        }
      ]
    });`;
    this.alertConfirm = `$.sweetModal.confirm('Confirm please?', function() {
      $.sweetModal('Thanks for confirming!');
    });`;
    this.alert = `$.sweetModal('This is a simple alert.');`;
    this.alertConfirmTitle = `$.sweetModal.confirm('Confirm alert message', 'Are you Confirm please?', function() {
        $.sweetModal('Thank you for confirming.');
      },
      function() {
        $.sweetModal('You are not Confirm. Try again.');
      }
    );`;
    this.promptValue = `$.sweetModal.prompt('Can I help you?', 'Can I?', 'Just like error solutions', function(val) {
      $.sweetModal('You typed: ' + val);
    });`;
    this.alertVideo = `$.sweetModal({
      title: 'How to work with Sweet Alert?',
      content: 'https://www.youtube.com/watch?v=bn5RaUVoel4',
      theme: $.sweetModal.THEME_DARK
    });`;
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/sweetmodal.min.js').then(function (sweetModal) {
      sweetModal();
    });
  }
}