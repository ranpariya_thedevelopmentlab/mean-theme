import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-animate-css',
  templateUrl: './animate-css.component.html',
  styleUrls: ['./animate-css.component.css']
})
export class AnimateCssComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="AnimateCSS";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/animate-css.min.js').then(function (animateCSS) {
      animateCSS();
    });
  }
}