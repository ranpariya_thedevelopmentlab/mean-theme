import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-sliders',
  templateUrl: './sliders.component.html',
  styleUrls: ['./sliders.component.css']
})
export class SlidersComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Plugins";
    globals.childMenu ="Sliders";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/slider.min.js').then(function (slider) {
      slider();
    });
  }
}