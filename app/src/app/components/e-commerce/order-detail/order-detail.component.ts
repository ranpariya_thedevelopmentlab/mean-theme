import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ECommerce";
    globals.childMenu ="OrderDetail";
  }

  ngOnInit() {
  }
}