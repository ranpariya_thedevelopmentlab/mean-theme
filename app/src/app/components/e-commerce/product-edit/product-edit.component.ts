import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ECommerce";
    globals.childMenu ="ProductEdit";
  }

  ngOnInit() {
    $('#product-des-edit').summernote({ minHeight: 180 });
    $(".group-dropdown").select2({
      minimumResultsForSearch: Infinity
    });
  }
}