import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ECommerce";
    globals.childMenu ="ProductDetail";
  }

  ngOnInit() {
    setTimeout(function(){
      $(".product-image").zoomToo({ magnify: 1 });

      $(".product-image-list > img").click(function() {
        var _target = $(this).parents('.product-detail').find('.product-image img');
        _target.attr("src", $(this).attr("src"));
      });
    },500);
  }
}