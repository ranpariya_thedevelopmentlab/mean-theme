import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ECommerce";
    globals.childMenu ="OrdersList";
  }

  ngOnInit() {
  	$('.orderlist-table').footable();
  }
}