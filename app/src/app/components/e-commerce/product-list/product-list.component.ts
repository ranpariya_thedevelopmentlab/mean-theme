import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ECommerce";
    globals.childMenu ="ProductList";
  }

  ngOnInit() {
  	$('.productlist-table').footable();
  }
}