import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-product-box',
  templateUrl: './product-box.component.html',
  styleUrls: ['./product-box.component.css']
})
export class ProductBoxComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ECommerce";
    globals.childMenu ="ProductsBox";
  }

  ngOnInit() {
    $(".product-like").click(function() {
      $(this).find("i").toggleClass("fa-heart-o").toggleClass("fa-heart").toggleClass("text-theme");
    });
  }
}