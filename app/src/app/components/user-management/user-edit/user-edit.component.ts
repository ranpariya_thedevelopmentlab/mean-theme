import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="UserManagement";
    globals.childMenu ="UserEdit";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/user-edit.min.js').then(function (userEdit) {
      userEdit();
    });
  }
}