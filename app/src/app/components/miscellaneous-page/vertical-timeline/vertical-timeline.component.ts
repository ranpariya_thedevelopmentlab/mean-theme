import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-vertical-timeline',
  templateUrl: './vertical-timeline.component.html',
  styleUrls: ['./vertical-timeline.component.css']
})
export class VerticalTimelineComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="MiscellaneousPage";
    globals.childMenu ="VerticalTimeline";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/vertical-timeline.min.js').then(function (verticleTimeline) {
      verticleTimeline();
    });
  }
}