import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="MiscellaneousPage";
    globals.childMenu ="SearchPage";
  }

  ngOnInit() {
  }
}