import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-video-page',
  templateUrl: './video-page.component.html',
  styleUrls: ['./video-page.component.css']
})
export class VideoPageComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="MiscellaneousPage";
    globals.childMenu ="VideoPage";
  }

  ngOnInit() {
  }
}