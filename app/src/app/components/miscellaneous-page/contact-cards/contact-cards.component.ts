import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-contact-cards',
  templateUrl: './contact-cards.component.html',
  styleUrls: ['./contact-cards.component.css']
})
export class ContactCardsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="MiscellaneousPage";
    globals.childMenu ="ContactCards";
  }

  ngOnInit() {
  }
}