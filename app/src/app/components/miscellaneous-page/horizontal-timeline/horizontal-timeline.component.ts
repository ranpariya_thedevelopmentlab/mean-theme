import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-horizontal-timeline',
  templateUrl: './horizontal-timeline.component.html',
  styleUrls: ['./horizontal-timeline.component.css']
})
export class HorizontalTimelineComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="MiscellaneousPage";
    globals.childMenu ="HorizontalTimeline";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/horizontal-timeline.min.js').then(function (horizontalTimeline) {
      horizontalTimeline();
    });
  }
}