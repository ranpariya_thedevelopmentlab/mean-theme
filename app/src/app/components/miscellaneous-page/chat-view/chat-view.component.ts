import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-chat-view',
  templateUrl: './chat-view.component.html',
  styleUrls: ['./chat-view.component.css']
})
export class ChatViewComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="MiscellaneousPage";
    globals.childMenu ="ChatView";
  }

  ngOnInit() {
  }
}