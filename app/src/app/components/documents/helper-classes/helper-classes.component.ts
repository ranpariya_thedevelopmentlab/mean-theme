import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-helper-classes',
  templateUrl: './helper-classes.component.html',
  styleUrls: ['./helper-classes.component.css']
})
export class HelperClassesComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Documents";
    globals.childMenu ="HelperClasses";
  }

  ngOnInit() {
  }
}