import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-bootstrap-grid',
  templateUrl: './bootstrap-grid.component.html',
  styleUrls: ['./bootstrap-grid.component.css']
})
export class BootstrapGridComponent implements OnInit {
  gridlayoutEqualWidth : string;
  gridlayoutSettingOneColumnWidth : string;
  gridlayoutVariableWidthContent : string;
  gridlayoutEqualWidthMultiRow : string;
  gridlayoutAllBreakpoints : string;
  gridlayoutStackedToHorizontal : string;
  gridlayoutMixAndMatch : string;
  gridlayoutColumnWrapping : string;
  gridlayoutColumnResets : string;
  gridlayoutFlexOrder : string;
  gridlayoutOffsettingColumns : string;
  gridlayoutNesting : string;
  constructor(private globals: Globals) {
    globals.parentMenu ="Documents";
    globals.childMenu ="BootstrapGrid";

    this.gridlayoutEqualWidth = `<div class="row">
  <div class="col">
    1 of 2
  </div>
  <div class="col">
    1 of 2
  </div>
</div>
<div class="row">
  <div class="col">
    1 of 3
  </div>
  <div class="col">
    1 of 3
  </div>
  <div class="col">
    1 of 3
  </div>
</div>`;

   this.gridlayoutSettingOneColumnWidth = `<div class="row">
  <div class="col">
    1 of 3
  </div>
  <div class="col-6">
    2 of 3 (wider)
  </div>
  <div class="col">
    3 of 3
  </div>
</div>
<div class="row">
  <div class="col">
    1 of 3
  </div>
  <div class="col-5">
    2 of 3 (wider)
  </div>
  <div class="col">
    3 of 3
  </div>
</div>`;

  this.gridlayoutVariableWidthContent = `<div class="row justify-content-md-center">
  <div class="col col-lg-2">
    1 of 3
  </div>
  <div class="col-12 col-md-auto">
    Variable width content
  </div>
  <div class="col col-lg-2">
    3 of 3
  </div>
</div>
<div class="row">
  <div class="col">
    1 of 3
  </div>
  <div class="col-12 col-md-auto">
    Variable width content
  </div>
  <div class="col col-lg-2">
    3 of 3
  </div>
</div>`;

  this.gridlayoutEqualWidthMultiRow = `<div class="row">
  <div class="col">col</div>
  <div class="col">col</div>
  <div class="w-100"></div>
  <div class="col">col</div>
  <div class="col">col</div>
</div>`;

  this.gridlayoutAllBreakpoints = `<div class="row">
  <div class="col">col</div>
  <div class="col">col</div>
  <div class="col">col</div>
  <div class="col">col</div>
</div>
<div class="row">
  <div class="col-8">col-8</div>
  <div class="col-4">col-4</div>
</div>`;

  this.gridlayoutStackedToHorizontal =`<div class="row">
  <div class="col-sm-8">col-sm-8</div>
  <div class="col-sm-4">col-sm-4</div>
</div>
<div class="row">
  <div class="col-sm">col-sm</div>
  <div class="col-sm">col-sm</div>
  <div class="col-sm">col-sm</div>
</div>`;

  this.gridlayoutMixAndMatch = `<!-- Stack the columns on mobile by making one full-width and the other half-width -->
<div class="row">
  <div class="col col-md-8">.col .col-md-8</div>
  <div class="col-6 col-md-4">.col-6 .col-md-4</div>
</div>

<!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
<div class="row">
  <div class="col-6 col-md-4">.col-6 .col-md-4</div>
  <div class="col-6 col-md-4">.col-6 .col-md-4</div>
  <div class="col-6 col-md-4">.col-6 .col-md-4</div>
</div>

<!-- Columns are always 50% wide, on mobile and desktop -->
<div class="row">
  <div class="col-6">.col-6</div>
  <div class="col-6">.col-6</div>
</div>`;

  this.gridlayoutColumnWrapping = `<div class="row">
  <div class="col-9">.col-9</div>
  <div class="col-4">.col-4<br>Since 9 + 4 = 13 &gt; 12, this 4-column-wide div gets wrapped onto a new line as one contiguous unit.</div>
  <div class="col-6">.col-6<br>Subsequent columns continue along the new line.</div>
</div>`;

  this.gridlayoutColumnResets = `<div class="row">
  <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
  <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix hidden-sm-up"></div>

  <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
  <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
</div>`;

  this.gridlayoutFlexOrder = `<div class="container">
  <div class="row">
    <div class="col flex-unordered">
      First, but unordered
    </div>
    <div class="col flex-last">
      Second, but last
    </div>
    <div class="col flex-first">
      Third, but first
    </div>
  </div>
</div>`;

  this.gridlayoutOffsettingColumns = `<div class="row">
  <div class="col-md-4">.col-md-4</div>
  <div class="col-md-4 offset-md-4">.col-md-4 .offset-md-4</div>
</div>
<div class="row">
  <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
  <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
</div>
<div class="row">
  <div class="col-md-6 offset-md-3">.col-md-6 .offset-md-3</div>
</div>`;

  this.gridlayoutNesting = `<div class="row">
  <div class="col-sm-9">
    Level 1: .col-sm-9
    <div class="row">
      <div class="col-8 col-sm-6">
        Level 2: .col-8 .col-sm-6
      </div>
      <div class="col-4 col-sm-6">
        Level 2: .col-4 .col-sm-6
      </div>
    </div>
  </div>
</div>`;
}

  ngOnInit() {
  }
}