import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';

@Component({
  selector: 'app-setting-menu',
  templateUrl: './setting-menu.component.html',
  styleUrls: ['./setting-menu.component.css']
})
export class SettingMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/setting-menu.min.js').then(function (settingMenu) {
      settingMenu();
    });
  }
}