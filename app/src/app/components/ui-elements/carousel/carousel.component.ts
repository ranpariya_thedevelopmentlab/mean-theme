import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  carouselSnippet : string;
  constructor(private globals: Globals) { 
    globals.parentMenu ="UIElements";
    globals.childMenu ="Carousel";

    this.carouselSnippet = `<ngb-carousel>
  <ng-template ngbSlide>
    <img class="d-block img-fluid w-100" src="..." alt="...">
    <div class="carousel-caption">
      <h3>First slide label</h3>
      <p>Lorem Ipsum is simply dummy text.</p>
    </div>
  </ng-template>
  <ng-template ngbSlide>
    <img class="d-block img-fluid w-100" src="..." alt="...">
    <div class="carousel-caption">
      <h3>Second slide label</h3>
      <p>Lorem Ipsum is simply dummy text.</p>
    </div>
  </ng-template>
  <ng-template ngbSlide>
    <img class="d-block img-fluid w-100" src="..." alt="...">
    <div class="carousel-caption">
      <h3>Third slide label</h3>
      <p>Lorem Ipsum is simply dummy text.</p>
    </div>
  </ng-template>
</ngb-carousel>`;
  }

  ngOnInit() {
  	$('.carousel').carousel();
  }
}