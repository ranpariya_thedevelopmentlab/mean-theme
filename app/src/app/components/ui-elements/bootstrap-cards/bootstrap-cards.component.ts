import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-bootstrap-cards',
  templateUrl: './bootstrap-cards.component.html',
  styleUrls: ['./bootstrap-cards.component.css']
})
export class BootstrapCardsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="UIElements";
    globals.childMenu ="BootstrapCards";
  }

  ngOnInit() {
  }
}