import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-scrollspy',
  templateUrl: './scrollspy.component.html',
  styleUrls: ['./scrollspy.component.css']
})
export class ScrollspyComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="UIElements";
    globals.childMenu ="Scrollspy";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/scrollspy.min.js').then(function (scrollspy) {
      scrollspy();
    });
  }
}