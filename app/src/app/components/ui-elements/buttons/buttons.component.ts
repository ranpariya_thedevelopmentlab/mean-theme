import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.css']
})
export class ButtonsComponent implements OnInit {

  model = {
    left: true,
    middle: false,
    right: false
  };
  constructor(private globals: Globals) {
    globals.parentMenu ="UIElements";
    globals.childMenu ="Buttons";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/components.min.js').then(function (components) {
      components();
    });
  }
}