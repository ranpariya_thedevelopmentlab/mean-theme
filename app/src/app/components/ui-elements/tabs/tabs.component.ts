import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

	currentJustify = 'start';
  constructor(private globals: Globals) {
    globals.parentMenu ="UIElements";
    globals.childMenu ="Tabs";
  }

  ngOnInit() {
  }
}