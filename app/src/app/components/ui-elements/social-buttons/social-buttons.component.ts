import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-social-buttons',
  templateUrl: './social-buttons.component.html',
  styleUrls: ['./social-buttons.component.css']
})
export class SocialButtonsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="UIElements";
    globals.childMenu ="SocialButtons";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/social-button.min.js').then(function (socialButton) {
      socialButton();
    });
  }
}