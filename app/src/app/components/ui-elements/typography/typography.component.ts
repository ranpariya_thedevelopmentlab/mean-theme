import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.css']
})
export class TypographyComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="UIElements";
    globals.childMenu ="Typography";
  }

  ngOnInit() {
  }
}