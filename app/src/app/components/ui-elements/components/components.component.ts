import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.css']
})
export class ComponentsComponent implements OnInit {

  closed = false;
  constructor(private globals: Globals) {
    globals.parentMenu ="UIElements";
    globals.childMenu ="Components";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/animate.min.js').then(function (animateJS) {
      animateJS();
    });
  }
}