import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDialogsComponent } from './modal-dialogs.component';

describe('ModalDialogsComponent', () => {
  let component: ModalDialogsComponent;
  let fixture: ComponentFixture<ModalDialogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDialogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDialogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
