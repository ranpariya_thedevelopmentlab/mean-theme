import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-rickshaw-charts',
  templateUrl: './rickshaw-charts.component.html',
  styleUrls: ['./rickshaw-charts.component.css']
})
export class RickshawChartsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Charts";
    globals.childMenu ="RickshawCharts";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/rickshaw-chart.min.js').then(function (rickshawChart) {
      rickshawChart();
    });
  }
}