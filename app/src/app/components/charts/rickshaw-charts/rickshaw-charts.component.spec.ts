import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RickshawChartsComponent } from './rickshaw-charts.component';

describe('RickshawChartsComponent', () => {
  let component: RickshawChartsComponent;
  let fixture: ComponentFixture<RickshawChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RickshawChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RickshawChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
