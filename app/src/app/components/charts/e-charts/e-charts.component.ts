import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-e-charts',
  templateUrl: './e-charts.component.html',
  styleUrls: ['./e-charts.component.css']
})
export class EChartsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Charts";
    globals.childMenu ="ECharts";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/echarts-config.min.js').then(function (eChart) {
      eChart();
    });
  }
}