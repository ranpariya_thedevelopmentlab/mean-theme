import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-chartjs-charts',
  templateUrl: './chartjs-charts.component.html',
  styleUrls: ['./chartjs-charts.component.css']
})
export class ChartjsChartsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Charts";
    globals.childMenu ="ChartJSCharts";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/chartJs-config.min.js').then(function (chartJsChart) {
      chartJsChart();
    });
  }
}