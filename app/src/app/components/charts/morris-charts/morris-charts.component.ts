import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-morris-charts',
  templateUrl: './morris-charts.component.html',
  styleUrls: ['./morris-charts.component.css']
})
export class MorrisChartsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Charts";
    globals.childMenu ="MorrisCharts";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/morris-chart-config.min.js').then(function (morrisChart) {
      morrisChart();
    });
  }
}