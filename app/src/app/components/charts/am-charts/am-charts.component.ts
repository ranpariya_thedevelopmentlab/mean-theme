import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-am-charts',
  templateUrl: './am-charts.component.html',
  styleUrls: ['./am-charts.component.css']
})
export class AmChartsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Charts";
    globals.childMenu ="AMCharts";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/amcharts-config.min.js').then(function (AMChart) {
      AMChart();
    });
  }
}