import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeityAndSparklineChartComponent } from './peity-and-sparkline-chart.component';

describe('PeityAndSparklineChartComponent', () => {
  let component: PeityAndSparklineChartComponent;
  let fixture: ComponentFixture<PeityAndSparklineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeityAndSparklineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeityAndSparklineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
