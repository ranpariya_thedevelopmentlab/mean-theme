import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-peity-and-sparkline-chart',
  templateUrl: './peity-and-sparkline-chart.component.html',
  styleUrls: ['./peity-and-sparkline-chart.component.css']
})
export class PeityAndSparklineChartComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Charts";
    globals.childMenu ="PeityAndSparklineCharts";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/peity-and-sparkline-chart-config.min.js').then(function (peityAndSparklineChart) {
      peityAndSparklineChart();
    });
  }
}