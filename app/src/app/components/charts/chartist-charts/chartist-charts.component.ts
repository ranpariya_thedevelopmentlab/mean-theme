import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-chartist-charts',
  templateUrl: './chartist-charts.component.html',
  styleUrls: ['./chartist-charts.component.css']
})
export class ChartistChartsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Charts";
    globals.childMenu ="ChartistCharts";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/chartist-charts.min.js').then(function (chartistChart) {
      chartistChart();
    });
  }
}