import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-flot-charts',
  templateUrl: './flot-charts.component.html',
  styleUrls: ['./flot-charts.component.css']
})
export class FlotChartsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Charts";
    globals.childMenu ="FlotCharts";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/flot-chart-config.min.js').then(function (flotChart) {
      flotChart();
    });
  }
}