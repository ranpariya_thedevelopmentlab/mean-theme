import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';

@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.css']
})
export class RightSidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/right-sidebar.min.js').then(function (rightSidebar) {
      rightSidebar();
    });
  }
}