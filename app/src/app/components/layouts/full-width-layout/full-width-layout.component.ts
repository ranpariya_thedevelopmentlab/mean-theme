import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-full-width-layout',
  templateUrl: './full-width-layout.component.html',
  styleUrls: ['./full-width-layout.component.css']
})
export class FullWidthLayoutComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Layouts";
    globals.childMenu ="FullwidthLayout";
  }

  ngOnInit() {
  }
}