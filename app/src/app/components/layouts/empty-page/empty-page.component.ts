import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-empty-page',
  templateUrl: './empty-page.component.html',
  styleUrls: ['./empty-page.component.css']
})
export class EmptyPageComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Layouts";
    globals.childMenu ="EmptyPage";
  }

  ngOnInit() {
  }
}