import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoizontalLayoutComponent } from './hoizontal-layout.component';

describe('HoizontalLayoutComponent', () => {
  let component: HoizontalLayoutComponent;
  let fixture: ComponentFixture<HoizontalLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoizontalLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoizontalLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
