import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';
import * as SystemJS from 'systemjs/dist/system';

@Component({
  selector: 'app-hoizontal-layout',
  templateUrl: './hoizontal-layout.component.html',
  styleUrls: ['./hoizontal-layout.component.css']
})
export class HoizontalLayoutComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Layouts";
    globals.childMenu ="HorizontalLayout";
  }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/horizontal-menu.min.js').then(function (horizontalLayout) {
      horizontalLayout();
    });
    SystemJS.import('/assets/dist/js/social-widgets.min.js').then(function (socialWidgets) {
      socialWidgets();
    });

    SystemJS.import('/assets/dist/js/main-dashboard.min.js').then(function (mainDashboard) {
      mainDashboard();
    });
  }
}