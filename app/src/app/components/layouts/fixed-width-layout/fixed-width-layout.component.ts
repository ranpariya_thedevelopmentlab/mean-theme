import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-fixed-width-layout',
  templateUrl: './fixed-width-layout.component.html',
  styleUrls: ['./fixed-width-layout.component.css']
})
export class FixedWidthLayoutComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Layouts";
    globals.childMenu ="FixedWidthLayout";
  }

  ngOnInit() {
    $('.box_checkbox').parent().addClass('checked disabled');
    $('.box_checkbox').attr('checked', true);
    $('.box_checkbox').attr('disabled', true);
  }
}