import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedWidthLayoutComponent } from './fixed-width-layout.component';

describe('FixedWidthLayoutComponent', () => {
  let component: FixedWidthLayoutComponent;
  let fixture: ComponentFixture<FixedWidthLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedWidthLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedWidthLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
