import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';
import * as SystemJS from 'systemjs/dist/system';

@Component({
  selector: 'app-sidebar-v2',
  templateUrl: './sidebar-v2.component.html',
  styleUrls: ['./sidebar-v2.component.css']
})
export class SidebarV2Component implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Layouts";
    globals.childMenu ="SidebarV2";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/sidebar-v2.min.js').then(function (sidebarV2) {
      sidebarV2();
    });
    SystemJS.import('/assets/dist/js/animate.min.js').then(function (animateJS) {
      animateJS();
    });
    SystemJS.import('/assets/dist/js/right-sidebar.min.js').then(function (rightSidebar) {
      rightSidebar();
    });
    SystemJS.import('/assets/dist/js/setting-menu.min.js').then(function (settingMenu) {
      settingMenu();
    });
    SystemJS.import('/assets/dist/js/custom-scrollbar.min.js').then(function (customScroll) {
      customScroll();
    });
    SystemJS.import('/assets/dist/js/scroll-top.min.js').then(function (scrollTop) {
      scrollTop();
    });
  }
}