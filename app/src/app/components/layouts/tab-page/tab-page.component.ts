import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-tab-page',
  templateUrl: './tab-page.component.html',
  styleUrls: ['./tab-page.component.css']
})
export class TabPageComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Layouts";
    globals.childMenu ="TabPage";
  }

  ngOnInit() {
  }
}