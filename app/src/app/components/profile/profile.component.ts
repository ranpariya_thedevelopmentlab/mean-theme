import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Profile";
    globals.childMenu ="Profile";
  }

  ngOnInit() {
    $('.square input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
    });
  }
}