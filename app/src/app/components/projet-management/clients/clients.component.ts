import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ProjectManagement";
    globals.childMenu ="Clients";
  }

  ngOnInit() {
  }
}