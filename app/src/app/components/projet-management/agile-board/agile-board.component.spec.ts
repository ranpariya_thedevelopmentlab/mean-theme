import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgileBoardComponent } from './agile-board.component';

describe('AgileBoardComponent', () => {
  let component: AgileBoardComponent;
  let fixture: ComponentFixture<AgileBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgileBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgileBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
