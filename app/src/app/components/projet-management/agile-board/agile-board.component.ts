import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-agile-board',
  templateUrl: './agile-board.component.html',
  styleUrls: ['./agile-board.component.css']
})
export class AgileBoardComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ProjectManagement";
    globals.childMenu ="AgileBoard";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/sortble-list.min.js').then(function (sortbleList) {
      sortbleList();
    });

    SystemJS.import('/assets/dist/js/agile-board.min.js').then(function (agileBoard) {
      agileBoard();
    });
  }
}