import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ProjectManagement";
    globals.childMenu ="ProjectList";
  }

  ngOnInit() {
  }
}