import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ProjectManagement";
    globals.childMenu ="ProjectDetail";
  }

  ngOnInit() {
  }
}