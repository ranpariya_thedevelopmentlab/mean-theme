import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.css']
})
export class IssueDetailComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="ProjectManagement";
    globals.childMenu ="IssueDetail";
  }

  ngOnInit() {
  }
}