import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-form-elements',
  templateUrl: './form-elements.component.html',
  styleUrls: ['./form-elements.component.css']
})
export class FormElementsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="FormsAndPlugins";
    globals.childMenu ="FormElements";
  }

  ngOnInit() {
  }
}