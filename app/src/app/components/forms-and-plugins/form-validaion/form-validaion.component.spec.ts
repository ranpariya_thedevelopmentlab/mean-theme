import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormValidaionComponent } from './form-validaion.component';

describe('FormValidaionComponent', () => {
  let component: FormValidaionComponent;
  let fixture: ComponentFixture<FormValidaionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormValidaionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormValidaionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
