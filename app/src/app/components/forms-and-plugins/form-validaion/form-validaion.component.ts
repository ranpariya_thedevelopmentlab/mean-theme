import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-form-validaion',
  templateUrl: './form-validaion.component.html',
  styleUrls: ['./form-validaion.component.css']
})
export class FormValidaionComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="FormsAndPlugins";
    globals.childMenu ="FormValidation";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/form-validation.min.js').then(function (formValidation) {
      formValidation();
    });
  }
}