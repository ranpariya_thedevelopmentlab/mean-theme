import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAdvanceElementsComponent } from './form-advance-elements.component';

describe('FormAdvanceElementsComponent', () => {
  let component: FormAdvanceElementsComponent;
  let fixture: ComponentFixture<FormAdvanceElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAdvanceElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAdvanceElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
