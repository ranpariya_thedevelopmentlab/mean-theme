import { Component, OnInit, Injectable} from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

import {NgbDateStruct, NgbCalendar, NgbDateAdapter, NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';

//  Date Range Picker
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

// Disabled datepicker   
const now = new Date();

@Component({
  selector: 'app-form-advance-elements',
  templateUrl: './form-advance-elements.component.html',
  styleUrls: ['./form-advance-elements.component.css'],
  providers: [NgbDatepickerConfig] // add NgbDatepickerConfig to the component providers
})
export class FormAdvanceElementsComponent implements OnInit {

  // Clock Picker
  time = {hour: 13, minute: 30, second: 30};
  meridian = true;
  seconds = true;

  // Date Range Picker
  hoveredDate: NgbDateStruct;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;

  constructor(private globals: Globals, calendar: NgbCalendar, config: NgbDatepickerConfig) {
    globals.parentMenu ="FormsAndPlugins";
    globals.childMenu ="FormAdvancedComponents";

    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);

    // Global configuration of datepickers
    // customize default values of datepickers used by this component tree
    config.minDate = {year: 1900, month: 1, day: 1};
    config.maxDate = {year: 2099, month: 12, day: 31};

    // days that don't belong to current month are not visible
    config.outsideDays = 'hidden';
  }

  // Date Range Picker
  onDateChange(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }
  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);

  // Disabled datepicker
  model: NgbDateStruct = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  disabled = true;

  // Custom day view
  model2: NgbDateStruct;
  isWeekend(date: NgbDateStruct) {
    const d = new Date(date.year, date.month - 1, date.day);
    return d.getDay() === 0 || d.getDay() === 6;
  }
  isDisabled(date: NgbDateStruct, current: {month: number}) {
    return date.month !== current.month;
  }

  // Global configuration of datepickers
  model1

  ngOnInit() {
    SystemJS.import('/assets/dist/js/cropper.min.js').then(function (cropper) {
      cropper();
    });
    SystemJS.import('/assets/dist/js/form-advance-element.min.js').then(function (formAdvanceElement) {
      formAdvanceElement();
    });
    SystemJS.import('/assets/dist/js/bootstrap-switch-config.min.js').then(function (bootstrapSwitch) {
      bootstrapSwitch();
    });
  }
}