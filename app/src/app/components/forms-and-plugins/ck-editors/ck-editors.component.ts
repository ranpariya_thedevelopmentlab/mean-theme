import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-ck-editors',
  templateUrl: './ck-editors.component.html',
  styleUrls: ['./ck-editors.component.css']
})
export class CkEditorsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="FormsAndPlugins";
    globals.childMenu ="CKEditor";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/ck-editors.min.js').then(function (ckEditor) {
      ckEditor();
    });
  }
}