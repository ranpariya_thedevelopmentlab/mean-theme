import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CkEditorsComponent } from './ck-editors.component';

describe('CkEditorsComponent', () => {
  let component: CkEditorsComponent;
  let fixture: ComponentFixture<CkEditorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CkEditorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CkEditorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
