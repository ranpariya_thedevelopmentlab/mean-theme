import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-summernote-editors',
  templateUrl: './summernote-editors.component.html',
  styleUrls: ['./summernote-editors.component.css']
})
export class SummernoteEditorsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="FormsAndPlugins";
    globals.childMenu ="SummernoteEditor";
  }

  ngOnInit() {
    // --
    // Summer note
    $('.summernote-editor').summernote();
  }
}