import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummernoteEditorsComponent } from './summernote-editors.component';

describe('SummernoteEditorsComponent', () => {
  let component: SummernoteEditorsComponent;
  let fixture: ComponentFixture<SummernoteEditorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummernoteEditorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummernoteEditorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
