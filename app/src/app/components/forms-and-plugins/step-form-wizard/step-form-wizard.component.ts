import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-step-form-wizard',
  templateUrl: './step-form-wizard.component.html',
  styleUrls: ['./step-form-wizard.component.css']
})
export class StepFormWizardComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="FormsAndPlugins";
    globals.childMenu ="StepFormWizards";
  }

  ngOnInit() {
    SystemJS.import('/assets/dist/js/form-wizard.min.js').then(function (formWizard) {
      formWizard();
    });
  }
}