import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepFormWizardComponent } from './step-form-wizard.component';

describe('StepFormWizardComponent', () => {
  let component: StepFormWizardComponent;
  let fixture: ComponentFixture<StepFormWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepFormWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepFormWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
