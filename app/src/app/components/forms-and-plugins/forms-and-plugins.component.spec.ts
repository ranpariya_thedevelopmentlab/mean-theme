import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsAndPluginsComponent } from './forms-and-plugins.component';

describe('FormsAndPluginsComponent', () => {
  let component: FormsAndPluginsComponent;
  let fixture: ComponentFixture<FormsAndPluginsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsAndPluginsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsAndPluginsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
