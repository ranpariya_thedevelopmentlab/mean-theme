import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private globals: Globals, private http: HttpClient, private _router: Router) {
    globals.parentMenu ="Pages";
    globals.childMenu ="ForgotPassword";
  }

  ngOnInit() {
  }

  forgot(forgotform) {
    this.http.post('/forgot-password', forgotform.value).subscribe(data => {
      this._router.navigateByUrl('/');
    });
  }
}