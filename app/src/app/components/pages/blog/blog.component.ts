import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Pages";
    globals.childMenu ="Blog";
  }

  ngOnInit() {
  }
}