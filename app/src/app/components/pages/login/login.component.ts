import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Globals } from '../../../../globals';
import { CookieService } from 'ngx-cookie';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private globals: Globals, private http: HttpClient, private _router: Router, private _cookieService:CookieService, public toastr: ToastsManager, vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);
    globals.parentMenu ="Pages";
    globals.childMenu ="Login";

  }

  ngOnInit() {
    $('.remember_me').iCheck({
      checkboxClass: 'icheckbox_theme',
    });
  }

  login(loginform) {
    this.http.post('/login', loginform.value).subscribe(data => {
      this._cookieService.putObject('user', data);
      this._router.navigateByUrl('/dashboard/main-dashboard');
    },
    error => {
      this.toastr.error('Login unsuccessfully!');
    });
  }
}