import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Globals } from '../../../../globals';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private globals: Globals, private http: HttpClient, private _router: Router, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    globals.parentMenu ="Pages";
    globals.childMenu ="Signup";
  }

  ngOnInit() {
  }

  signup(signupform) {
	this.http.post('/signup', signupform.value).subscribe(data => {
      this._router.navigateByUrl('/');
    },
    error => {
      this.toastr.error('Registration unsuccessfully!');
    });
  }
}