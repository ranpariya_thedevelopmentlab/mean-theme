import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
  token : string;
  constructor(private route: ActivatedRoute, private http: HttpClient, private _router: Router, public toastr: ToastsManager, vcr: ViewContainerRef) {
  	this.toastr.setRootViewContainerRef(vcr);
    this.route.params.subscribe( params => {
  		this.token = params.token;
  	});
  }

  ngOnInit() {
  }

  reset(resetform) {
  	this.http.post('/reset/' + this.token, resetform.value).subscribe(data => {
      this._router.navigateByUrl('/');
    },
    error => {
      this.toastr.error('Password change unsuccessfully!');
    });
  }

}
