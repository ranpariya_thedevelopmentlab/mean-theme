import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-vote-list',
  templateUrl: './vote-list.component.html',
  styleUrls: ['./vote-list.component.css']
})
export class VoteListComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Pages";
    globals.childMenu ="VoteList";
  }

  ngOnInit() {
  }
}