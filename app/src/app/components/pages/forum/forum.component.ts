import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Pages";
    globals.childMenu ="Forum";
  }

  ngOnInit() {
  }
}