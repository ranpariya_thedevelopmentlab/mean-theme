import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Pages";
    globals.childMenu ="BlogList";
  }

  ngOnInit() {
  }
}