import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie'
import { Globals } from '../../../../globals';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-lock-screen',
  templateUrl: './lock-screen.component.html',
  styleUrls: ['./lock-screen.component.css']
})
export class LockScreenComponent implements OnInit {

  user: any;
  constructor(private globals: Globals, private http: HttpClient, private _router: Router, private _cookieService:CookieService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    globals.parentMenu ="Pages";
    globals.childMenu ="LockScreen";
    globals.isLocked = true;

    if(this._cookieService.get('user')){
      if(this._cookieService.get('user').substr(0, 2) == 'j:') {
        this.user = JSON.parse(this._cookieService.get('user').replace('j:', ''));
      } else {
        this.user = JSON.parse(this._cookieService.get('user'));
      }
    }
  }

  ngOnInit() {
  }

  lockscreen(lockscreenform){
  	this.http.post('/lock-screen', lockscreenform.value).subscribe(data => {
      this.globals.isLocked = false;
      this._router.navigateByUrl('/dashboard/main-dashboard');
    },
    error => {
      this.toastr.error('Login unsuccessfully!');
    });
  }
}