import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-error-404',
  templateUrl: './error-404.component.html',
  styleUrls: ['./error-404.component.css']
})
export class Error404Component implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Pages";
    globals.childMenu ="404";
  }

  ngOnInit() {
  }
}