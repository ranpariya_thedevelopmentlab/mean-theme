import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-data-maps',
  templateUrl: './data-maps.component.html',
  styleUrls: ['./data-maps.component.css']
})
export class DataMapsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Maps";
    globals.childMenu ="DataMaps";
  }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/data-map.min.js').then(function (dataMap) {
      dataMap();
    });
  }
}