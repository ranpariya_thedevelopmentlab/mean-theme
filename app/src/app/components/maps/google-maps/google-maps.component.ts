import { Component, OnInit } from '@angular/core';
import * as SystemJS from 'systemjs/dist/system';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-google-maps',
  templateUrl: './google-maps.component.html',
  styleUrls: ['./google-maps.component.css']
})
export class GoogleMapsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Maps";
    globals.childMenu ="GoogleMaps";
  }

  ngOnInit() {
  	SystemJS.import('/assets/dist/js/google-maps.min.js').then(function (googleMap) {
      googleMap();
    });
  }
}