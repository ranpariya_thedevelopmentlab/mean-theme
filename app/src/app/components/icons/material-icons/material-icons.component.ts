import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-material-icons',
  templateUrl: './material-icons.component.html',
  styleUrls: ['./material-icons.component.css']
})
export class MaterialIconsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Icons";
    globals.childMenu ="MaterialIcons";
  }

  ngOnInit() {
  }
}