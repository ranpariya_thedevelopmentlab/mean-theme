import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-fontawesome-icons',
  templateUrl: './fontawesome-icons.component.html',
  styleUrls: ['./fontawesome-icons.component.css']
})
export class FontawesomeIconsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Icons";
    globals.childMenu ="FontAwesomeIcon";
  }

  ngOnInit() {
  }
}