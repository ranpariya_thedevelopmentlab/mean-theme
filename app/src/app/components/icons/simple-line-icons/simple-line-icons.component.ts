import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-simple-line-icons',
  templateUrl: './simple-line-icons.component.html',
  styleUrls: ['./simple-line-icons.component.css']
})
export class SimpleLineIconsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Icons";
    globals.childMenu ="SimpleLineIcons";
  }

  ngOnInit() {
  }
}