import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IoniconsIconsComponent } from './ionicons-icons.component';

describe('IoniconsIconsComponent', () => {
  let component: IoniconsIconsComponent;
  let fixture: ComponentFixture<IoniconsIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IoniconsIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IoniconsIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
