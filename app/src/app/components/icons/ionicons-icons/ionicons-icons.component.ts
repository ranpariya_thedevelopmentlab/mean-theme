import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../globals';

@Component({
  selector: 'app-ionicons-icons',
  templateUrl: './ionicons-icons.component.html',
  styleUrls: ['./ionicons-icons.component.css']
})
export class IoniconsIconsComponent implements OnInit {

  constructor(private globals: Globals) {
    globals.parentMenu ="Icons";
    globals.childMenu ="IoniconsIcons";
  }

  ngOnInit() {
  }
}