import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class NonAuthGuard implements CanActivate {
  constructor(private router: Router, private _cookieService:CookieService) { }
  canActivate(router: ActivatedRouteSnapshot, state:RouterStateSnapshot) {
    if(this._cookieService.get('user')) {
    this.router.navigateByUrl('/dashboard/main-dashboard');
      return false;
    }
    return true;
  }
}
