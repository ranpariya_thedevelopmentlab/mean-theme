import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private _cookieService:CookieService) { }
  canActivate(router: ActivatedRouteSnapshot, state:RouterStateSnapshot) {
    if(this._cookieService.get('user')) {
      return true;
    }
    this.router.navigate(['/pages/login'], { queryParams: {returnUrl:state.url}});
    return false;
  }
}
