import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { NgProgress } from '@ngx-progressbar/core';
import { CookieService } from 'ngx-cookie';
import { Globals } from '../globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  outerLayout :boolean;
  showLeftSidebar :boolean;
  defaultNavbar :boolean;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    location: Location,
    private renderer: Renderer2,
    public progress: NgProgress,
    private _cookieService:CookieService,
    private globals: Globals)
  {
    let authRoute = ['','/layouts/sidebar-v2','/pages/error-404', '/pages/error-500', '/pages/login', '/pages/signup', '/pages/lock-screen', '/pages/forgot-password', '/reset/:token'],
        islocked = ['/pages/lock-screen'],
        errorPageRoute = ['/pages/error-404', '/pages/error-500'],
        fixedWidthPageRoute = ['/layouts/fixed-width-layout'],
        fullWidthPageRoute = ['/layouts/full-width-layout', '/layouts/hoizontal-layout'],
        navbarV2 = ['/layouts/navbar-v2'];
    globals.isLocked;

    router.events.subscribe((val) => {

      // --
      // Auth Pages
      if(authRoute.indexOf(location.path()) !== -1  || location.path().indexOf('/reset/') !== -1) {
        this.outerLayout = false;
      } else {
        if(!this._cookieService.get('user')) {
          window.location.href = '/'
          this.outerLayout = false;
          return;
        } else {
          this.outerLayout = true;
        }
      }

      // --
      // Lock Screen
      if(islocked.indexOf(location.path()) == -1) {
        if(globals.isLocked == true) {
          this.outerLayout = false;
           window.location.href = '#/pages/lock-screen'
        }
      }

      // --
      // Navbar v2
      if(navbarV2.indexOf(location.path()) !== -1) {
        this.defaultNavbar = false;
      } else {
        this.defaultNavbar = true;
      }

      // --
      // Error Pages
      if(errorPageRoute.indexOf(location.path()) !== -1) {
        this.renderer.addClass(document.body, 'error-page');
      }
      else {
        this.renderer.removeClass(document.body, 'error-page');
      }

      // --
      // Fixed Width Layout Page
      if(fixedWidthPageRoute.indexOf(location.path()) !== -1) {
        this.renderer.addClass(document.body, 'boxed-layout');
      }
      else {
        if(window.localStorage.getItem("always-boxed-layout")) {
          this.renderer.addClass(document.body, 'boxed-layout');
        } else {
          this.renderer.removeClass(document.body, 'boxed-layout');
        }
      }

      // --
      // Full Width Layout Page
      if(fullWidthPageRoute.indexOf(location.path()) !== -1) {
        this.renderer.addClass(document.body, 'full-width-page');
        this.showLeftSidebar= false;
      }
      else {
        this.renderer.removeClass(document.body, 'full-width-page');
        this.showLeftSidebar= true;
      }
    });

    // --
    // Setting Menu Checkbox
    router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        // Fixed Header
        if(!window.localStorage.getItem("always-sticky-header")) {
          $('.header').removeClass('fixed-navbar');
          $('.navbar_checkbox').parent().removeClass('checked');
          $('.navbar_checkbox').attr('checked', false);
        }
        // Fixed Sidebar
        if(!window.localStorage.getItem("always-sticky-sidebar")) {
          $('.sidebar-v1').removeClass('fixed-sidebar');
          $('.header').removeClass('fixed-navbar');
          $('.sidebar_checkbox').parent().removeClass('checked');
          $('.navbar_checkbox').parent().removeClass('checked disabled');
          $('.sidebar_checkbox').attr('checked', false);
          $('.navbar_checkbox').attr('checked', false);
          $('.navbar_checkbox').attr('disabled', false);
        }
        // Fixed Footer
        if(!window.localStorage.getItem("always-sticky-footer")) {
          $('.footer').removeClass('fixed');
          $('.footer_checkbox').parent().removeClass('checked');
          $('.footer_checkbox').attr('checked', false);
        }
        // Fixed Boxed Layout
        if(!window.localStorage.getItem("always-boxed-layout")) {
          $('.box_checkbox').parent().removeClass('checked disabled');
          $('.box_checkbox').attr('checked', false);
          $('.box_checkbox').attr('disabled', false);
        }
        // Setting menu
        $('.theme-setting-menu').removeClass('theme-setting-menu-toggle');
      }
    });

    this.progress.started.subscribe(() => {
      $('.wrapper').addClass('d-none');
      $('.authPage').addClass('d-none');
    });

    this.progress.ended.subscribe(() => {
      $('.wrapper').removeClass('d-none');
      $('.authPage').removeClass('d-none');
    });
   }

  ngOnInit() {
    if(!this._cookieService.get('user')) {
      this.outerLayout = true;
      this.router.navigateByUrl('');
    }
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map(route => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
      .filter(route => route.outlet === 'primary')
      .mergeMap(route => route.data)
      .subscribe((event) => this.titleService.setTitle(event['title']));
  }
}