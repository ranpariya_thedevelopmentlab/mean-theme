import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, enableProdMode } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { ToastModule } from 'ng2-toastr/ng2-toastr';

// Cookie Module
import { CookieModule } from 'ngx-cookie';

// NG Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


// Gloabal Variable
import { Globals } from '../globals';

// Dropzone
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

// Show Hide Password Module
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

// Route Module
import { RouteRoutingModule } from "./route/route-routing.module";
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressRouterModule } from '@ngx-progressbar/router'

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { RightSidebarComponent } from './components/right-sidebar/right-sidebar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FooterComponent } from './components/footer/footer.component';
import { SettingMenuComponent } from './components/setting-menu/setting-menu.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DataAnalyticsDashboardComponent } from './components/dashboard/data-analytics-dashboard/data-analytics-dashboard.component';
import { UiElementsComponent } from './components/ui-elements/ui-elements.component';
import { ButtonsComponent } from './components/ui-elements/buttons/buttons.component';
import { MarketingDashboardComponent } from './components/dashboard/marketing-dashboard/marketing-dashboard.component';
import { SocialDashboardComponent } from './components/dashboard/social-dashboard/social-dashboard.component';
import { ProjectDashboardComponent } from './components/dashboard/project-dashboard/project-dashboard.component';
import { SalesDashboardComponent } from './components/dashboard/sales-dashboard/sales-dashboard.component';
import { MainDashboardComponent } from './components/dashboard/main-dashboard/main-dashboard.component';
import { BootstrapCardsComponent } from './components/ui-elements/bootstrap-cards/bootstrap-cards.component';
import { TabsComponent } from './components/ui-elements/tabs/tabs.component';
import { TypographyComponent } from './components/ui-elements/typography/typography.component';
import { ComponentsComponent } from './components/ui-elements/components/components.component';
import { SocialButtonsComponent } from './components/ui-elements/social-buttons/social-buttons.component';
import { CarouselComponent } from './components/ui-elements/carousel/carousel.component';
import { ModalDialogsComponent } from './components/ui-elements/modal-dialogs/modal-dialogs.component';
import { ScrollspyComponent } from './components/ui-elements/scrollspy/scrollspy.component';
import { WidgetsComponent } from './components/widgets/widgets.component';
import { SocialWidgetsComponent } from './components/widgets/social-widgets/social-widgets.component';
import { OtherWidgetsComponent } from './components/widgets/other-widgets/other-widgets.component';
import { WeatherWidgetsComponent } from './components/widgets/weather-widgets/weather-widgets.component';
import { MaterialCardsComponent } from './components/widgets/material-cards/material-cards.component';
import { ChartsComponent } from './components/charts/charts.component';
import { AmChartsComponent } from './components/charts/am-charts/am-charts.component';
import { ChartjsChartsComponent } from './components/charts/chartjs-charts/chartjs-charts.component';
import { GoogleChartsComponent } from './components/charts/google-charts/google-charts.component';
import { MorrisChartsComponent } from './components/charts/morris-charts/morris-charts.component';
import { FlotChartsComponent } from './components/charts/flot-charts/flot-charts.component';
import { RickshawChartsComponent } from './components/charts/rickshaw-charts/rickshaw-charts.component';
import { EChartsComponent } from './components/charts/e-charts/e-charts.component';
import { ChartistChartsComponent } from './components/charts/chartist-charts/chartist-charts.component';
import { PeityAndSparklineChartComponent } from './components/charts/peity-and-sparkline-chart/peity-and-sparkline-chart.component';
import { FormsAndPluginsComponent } from './components/forms-and-plugins/forms-and-plugins.component';
import { FormElementsComponent } from './components/forms-and-plugins/form-elements/form-elements.component';
import { FormAdvanceElementsComponent } from './components/forms-and-plugins/form-advance-elements/form-advance-elements.component';
import { StepFormWizardComponent } from './components/forms-and-plugins/step-form-wizard/step-form-wizard.component';
import { FileUploaderComponent } from './components/forms-and-plugins/file-uploader/file-uploader.component';
import { SummernoteEditorsComponent } from './components/forms-and-plugins/summernote-editors/summernote-editors.component';
import { CkEditorsComponent } from './components/forms-and-plugins/ck-editors/ck-editors.component';
import { FormValidaionComponent } from './components/forms-and-plugins/form-validaion/form-validaion.component';
import { PluginsComponent } from './components/plugins/plugins.component';
import { ToastrAlertComponent } from './components/plugins/toastr-alert/toastr-alert.component';
import { AnimateCssComponent } from './components/plugins/animate-css/animate-css.component';
import { TinyconComponent } from './components/plugins/tinycon/tinycon.component';
import { ClipboardComponent } from './components/plugins/clipboard/clipboard.component';
import { IntroJsComponent } from './components/plugins/intro-js/intro-js.component';
import { SlidersComponent } from './components/plugins/sliders/sliders.component';
import { SortableComponentsComponent } from './components/plugins/sortable-components/sortable-components.component';
import { SpinnerButtonsComponent } from './components/plugins/spinner-buttons/spinner-buttons.component';
import { SweetModalsComponent } from './components/plugins/sweet-modals/sweet-modals.component';
import { BarcodeGeneratorComponent } from './components/plugins/barcode-generator/barcode-generator.component';
import { PageLoadersComponent } from './components/plugins/page-loaders/page-loaders.component';
import { LiquidMeterComponent } from './components/plugins/liquid-meter/liquid-meter.component';
import { ImageViewerComponent } from './components/plugins/image-viewer/image-viewer.component';
import { SplitJsComponent } from './components/plugins/split-js/split-js.component';
import { RatingComponent } from './components/plugins/rating/rating.component';
import { MapsComponent } from './components/maps/maps.component';
import { GoogleMapsComponent } from './components/maps/google-maps/google-maps.component';
import { DataMapsComponent } from './components/maps/data-maps/data-maps.component';
import { LayoutsComponent } from './components/layouts/layouts.component';
import { EmptyPageComponent } from './components/layouts/empty-page/empty-page.component';
import { FullWidthLayoutComponent } from './components/layouts/full-width-layout/full-width-layout.component';
import { FixedWidthLayoutComponent } from './components/layouts/fixed-width-layout/fixed-width-layout.component';
import { TabPageComponent } from './components/layouts/tab-page/tab-page.component';
import { SidebarV2Component } from './components/layouts/sidebar-v2/sidebar-v2.component';
import { PagesComponent } from './components/pages/pages.component';
import { LoginComponent } from './components/pages/login/login.component';
import { SignupComponent } from './components/pages/signup/signup.component';
import { ForgotPasswordComponent } from './components/pages/forgot-password/forgot-password.component';
import { LockScreenComponent } from './components/pages/lock-screen/lock-screen.component';
import { Error404Component } from './components/pages/error-404/error-404.component';
import { Error500Component } from './components/pages/error-500/error-500.component';
import { BlogListComponent } from './components/pages/blog-list/blog-list.component';
import { BlogComponent } from './components/pages/blog/blog.component';
import { SupportsComponent } from './components/pages/supports/supports.component';
import { ArticleComponent } from './components/pages/article/article.component';
import { ForumComponent } from './components/pages/forum/forum.component';
import { VoteListComponent } from './components/pages/vote-list/vote-list.component';
import { TablesComponent } from './components/tables/tables.component';
import { FooTableComponent } from './components/tables/foo-table/foo-table.component';
import { JsGridTableComponent } from './components/tables/js-grid-table/js-grid-table.component';
import { DataTableComponent } from './components/tables/data-table/data-table.component';
import { BootstrapTablesComponent } from './components/tables/bootstrap-tables/bootstrap-tables.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { MailboxComponent } from './components/mailbox/mailbox.component';
import { InboxComponent } from './components/mailbox/inbox/inbox.component';
import { ReadmailComponent } from './components/mailbox/readmail/readmail.component';
import { ECommerceComponent } from './components/e-commerce/e-commerce.component';
import { ProductListComponent } from './components/e-commerce/product-list/product-list.component';
import { ProductEditComponent } from './components/e-commerce/product-edit/product-edit.component';
import { ProductBoxComponent } from './components/e-commerce/product-box/product-box.component';
import { ProductDetailComponent } from './components/e-commerce/product-detail/product-detail.component';
import { CartComponent } from './components/e-commerce/cart/cart.component';
import { OrderListComponent } from './components/e-commerce/order-list/order-list.component';
import { OrderDetailComponent } from './components/e-commerce/order-detail/order-detail.component';
import { PaymentsComponent } from './components/e-commerce/payments/payments.component';
import { InvoiceComponent } from './components/e-commerce/invoice/invoice.component';
import { ProjetManagementComponent } from './components/projet-management/projet-management.component';
import { ProjectListComponent } from './components/projet-management/project-list/project-list.component';
import { ProjectDetailComponent } from './components/projet-management/project-detail/project-detail.component';
import { ClientsComponent } from './components/projet-management/clients/clients.component';
import { IssueListComponent } from './components/projet-management/issue-list/issue-list.component';
import { IssueDetailComponent } from './components/projet-management/issue-detail/issue-detail.component';
import { ProjectBoardComponent } from './components/projet-management/project-board/project-board.component';
import { AgileBoardComponent } from './components/projet-management/agile-board/agile-board.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { UserListComponent } from './components/user-management/user-list/user-list.component';
import { UserEditComponent } from './components/user-management/user-edit/user-edit.component';
import { MiscellaneousPageComponent } from './components/miscellaneous-page/miscellaneous-page.component';
import { ContactCardsComponent } from './components/miscellaneous-page/contact-cards/contact-cards.component';
import { FaqComponent } from './components/miscellaneous-page/faq/faq.component';
import { ChatViewComponent } from './components/miscellaneous-page/chat-view/chat-view.component';
import { HorizontalTimelineComponent } from './components/miscellaneous-page/horizontal-timeline/horizontal-timeline.component';
import { VerticalTimelineComponent } from './components/miscellaneous-page/vertical-timeline/vertical-timeline.component';
import { PdfViewerComponent } from './components/miscellaneous-page/pdf-viewer/pdf-viewer.component';
import { PricingComponent } from './components/miscellaneous-page/pricing/pricing.component';
import { SearchPageComponent } from './components/miscellaneous-page/search-page/search-page.component';
import { VideoPageComponent } from './components/miscellaneous-page/video-page/video-page.component';
import { ImageGalleryComponent } from './components/image-gallery/image-gallery.component';
import { FileManagerComponent } from './components/file-manager/file-manager.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { BootstrapGridComponent } from './components/documents/bootstrap-grid/bootstrap-grid.component';
import { HelperClassesComponent } from './components/documents/helper-classes/helper-classes.component';
import { ScrollTopComponent } from './components/scroll-top/scroll-top.component';
import { PrismComponent } from 'angular-prism';
import { CardModule } from 'ngx-card/ngx-card';
import { NavbarV2Component } from './components/layouts/navbar-v2/navbar-v2.component';
import { HeaderV2Component } from './components/header-v2/header-v2.component';
import { HoizontalLayoutComponent } from './components/layouts/hoizontal-layout/hoizontal-layout.component';
import { IconsComponent } from './components/icons/icons.component';
import { FontawesomeIconsComponent } from './components/icons/fontawesome-icons/fontawesome-icons.component';
import { SimpleLineIconsComponent } from './components/icons/simple-line-icons/simple-line-icons.component';
import { IoniconsIconsComponent } from './components/icons/ionicons-icons/ionicons-icons.component';
import { MaterialIconsComponent } from './components/icons/material-icons/material-icons.component';
import { ResetComponent } from './components/pages/reset/reset.component';

import { AuthGuard, NonAuthGuard } from "./guard/guard"

// Enabled Prod Mode
enableProdMode();

// Dropzone
const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
 // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RightSidebarComponent,
    SidebarComponent,
    FooterComponent,
    SettingMenuComponent,
    DashboardComponent,
    DataAnalyticsDashboardComponent,
    UiElementsComponent,
    ButtonsComponent,
    MarketingDashboardComponent,
    SocialDashboardComponent,
    ProjectDashboardComponent,
    SalesDashboardComponent,
    MainDashboardComponent,
    BootstrapCardsComponent,
    TabsComponent,
    TypographyComponent,
    ComponentsComponent,
    SocialButtonsComponent,
    CarouselComponent,
    ModalDialogsComponent,
    ScrollspyComponent,
    WidgetsComponent,
    SocialWidgetsComponent,
    OtherWidgetsComponent,
    WeatherWidgetsComponent,
    MaterialCardsComponent,
    ChartsComponent,
    AmChartsComponent,
    ChartjsChartsComponent,
    GoogleChartsComponent,
    MorrisChartsComponent,
    FlotChartsComponent,
    RickshawChartsComponent,
    EChartsComponent,
    ChartistChartsComponent,
    PeityAndSparklineChartComponent,
    FormsAndPluginsComponent,
    FormElementsComponent,
    FormAdvanceElementsComponent,
    StepFormWizardComponent,
    FileUploaderComponent,
    SummernoteEditorsComponent,
    CkEditorsComponent,
    FormValidaionComponent,
    PluginsComponent,
    ToastrAlertComponent,
    AnimateCssComponent,
    TinyconComponent,
    ClipboardComponent,
    IntroJsComponent,
    SlidersComponent,
    SortableComponentsComponent,
    SpinnerButtonsComponent,
    SweetModalsComponent,
    BarcodeGeneratorComponent,
    PageLoadersComponent,
    LiquidMeterComponent,
    ImageViewerComponent,
    SplitJsComponent,
    RatingComponent,
    MapsComponent,
    GoogleMapsComponent,
    DataMapsComponent,
    LayoutsComponent,
    EmptyPageComponent,
    FullWidthLayoutComponent,
    FixedWidthLayoutComponent,
    TabPageComponent,
    SidebarV2Component,
    PagesComponent,
    LoginComponent,
    SignupComponent,
    ForgotPasswordComponent,
    LockScreenComponent,
    Error404Component,
    Error500Component,
    BlogListComponent,
    BlogComponent,
    SupportsComponent,
    ArticleComponent,
    ForumComponent,
    VoteListComponent,
    TablesComponent,
    FooTableComponent,
    JsGridTableComponent,
    DataTableComponent,
    BootstrapTablesComponent,
    CalendarComponent,
    MailboxComponent,
    InboxComponent,
    ReadmailComponent,
    ECommerceComponent,
    ProductListComponent,
    ProductEditComponent,
    ProductBoxComponent,
    ProductDetailComponent,
    CartComponent,
    OrderListComponent,
    OrderDetailComponent,
    PaymentsComponent,
    InvoiceComponent,
    ProjetManagementComponent,
    ProjectListComponent,
    ProjectDetailComponent,
    ClientsComponent,
    IssueListComponent,
    IssueDetailComponent,
    ProjectBoardComponent,
    AgileBoardComponent,
    UserManagementComponent,
    UserListComponent,
    UserEditComponent,
    MiscellaneousPageComponent,
    ContactCardsComponent,
    FaqComponent,
    ChatViewComponent,
    HorizontalTimelineComponent,
    VerticalTimelineComponent,
    PdfViewerComponent,
    PricingComponent,
    SearchPageComponent,
    VideoPageComponent,
    ImageGalleryComponent,
    FileManagerComponent,
    ProfileComponent,
    DocumentsComponent,
    BootstrapGridComponent,
    HelperClassesComponent,
    ScrollTopComponent,
    PrismComponent,
    NavbarV2Component,
    HeaderV2Component,
    HoizontalLayoutComponent,
    IconsComponent,
    FontawesomeIconsComponent,
    SimpleLineIconsComponent,
    IoniconsIconsComponent,
    MaterialIconsComponent,
    ResetComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouteRoutingModule,
    NgProgressModule.forRoot(),
    NgProgressRouterModule,
    CardModule,
    DropzoneModule,
    ShowHidePasswordModule,
    HttpClientModule,
    FormsModule,
    CookieModule.forRoot(),
    NgbModule.forRoot(),
    ReactiveFormsModule,
    ToastModule.forRoot()
  ],
  providers: [
    {
      // Dropzone
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    },
    Globals,
    AuthGuard, NonAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
