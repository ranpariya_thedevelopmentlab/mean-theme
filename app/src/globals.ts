import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  parentMenu: string = '';
  childMenu: string = '';
  isLocked: boolean;
}