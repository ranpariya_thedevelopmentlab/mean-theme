// ===========
// User model
// ===========

// MODULES =================================================
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt');


var UserSchema = new Schema({
    username: {
        type: String,
        trim: true
    },
    displayName: {
        type: String,
        trim: true
    },
    password: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true
    },
    firstname: {
        type: String,
        trim: true
    },
    lastname: {
        type: String,
        trim: true
    },
    resetPasswordToken: String,
    twitter: {
        id:{
            type: String,
            trim: true
        },
        token:{
            type: String
        },
        displayName:{
            type: String
        },
        username:{
            type: String
        }
    },
    facebook: {
        id:{
            type: String,
            trim: true
        },
        token:{
            type: String
        },
        displayName:{
            type: String
        },
        email:{
            type: String
        }
    }
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.crateUser = function(newUser, callback){
    bcrypt.genSalt(10, function(err, salt){
        bcrypt.hash(newUser.password, salt, function(err, hash){
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.comparePassword = function(candidatePassword, hash, callback) {  
    bcrypt.compare(candidatePassword, hash, function(err, isMatch) {        
        if(err) throw err;      
        callback(null, isMatch);    
    });
}

module.exports.getUserByUsername = function(username, callback) {
    var query = {username: username};
    User.findOne(query, callback);
}